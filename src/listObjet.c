#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../header/objet.h"
#include "../header/listObjet.h"
#include "../header/tool.h"


/** fonction qui lit une liste d'objet à partir d'un flux de données
*@param FILE* fichier : flux de donnée où on lit la liste
*@param int nbObjet : le nombre d'objet que doit contenir la liste
*@return objet* list : la structure liste lu dans le fichier
*/
objet* objet_read_valeur(FILE* fichier, int nbObjet){
    objet* list = (objet*)malloc((size_t)nbObjet*sizeof(objet));

    for(int i=0; i<nbObjet; i++){
        fscanf(fichier, "%d", &(list[i].valeur));
        list[i].listePoids = NULL;
    }

    return list;
}

/** fonction qui lit les poid/dimension des objets d'une liste à partir d'un flux de données
*@param FILE* fichier : flux de donnée où on lit la liste
*@param listObjet * list : la liste d'objet pour lesquelles on veut connaitre les poids/dimension
*@param int nbDimension : le nombre d'objet que doit contenir la liste
*@return objet* list : la structure liste lu dans le fichier
*/
void objet_read_poids(FILE* fichier, instance * row){
    int nbObjet = row->nbObjet;
    int * tab;
    for(int j = 0; j<nbObjet; ++j){
        (row->listeObj[j]).listePoids = (int *)malloc((size_t)(row->nbDimension)*sizeof(int));
    }
    for(int i=0; i<row->nbDimension; ++i){
        tab = read_tab(fichier, nbObjet);
        for(int j = 0; j<nbObjet; ++j){
            (row->listeObj[j]).listePoids[i] = tab[j];
        }
        free(tab);
    }
}
#include <stdio.h>
#include <stdlib.h>
#include "../header/objet.h"
#include "../header/instance.h"

/** fonction qui initialise un objet
*@param objet obj : objet à iniliatiliser
*/
void objet_init(objet* obj){
    obj->valeur = 0;
    obj->listePoids = NULL;
}

/** fonction qui déssaloue la mémoire inutile et détruit l'objet
* @param objet * obj : l'objet à détruire
*/
void objet_destroy(objet * obj){
   if(obj!=NULL){
        if(obj->listePoids!=NULL)
            free(obj->listePoids);
    }
}

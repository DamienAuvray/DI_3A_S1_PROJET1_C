#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../header/instance.h"
#include "../header/objet.h"
#include "../header/listObjet.h"
#include "../header/tool.h"


/** fonction qui creer et initialise une structure instance
*@return : instance * row : pointeur sur une structure instance initialisée.
*/
instance* instance_create_and_init(void){
    //initialisation d'une instance
    instance * row = (instance*)malloc(sizeof(instance));
    row->nbObjet=0;
    row->nbDimension=0;
    row->listeObj = NULL;
    row->poidMax = NULL;

    return row;
}


/** fonction qui détruit l'instance prise en paramètre et désalloue tout la mémoire nécessaire
* @param instance * row : instance à détruire
*/
void instance_destroy(instance * row){
    free(row->sol);
    for(int i = 0; i<row->nbObjet; i++){
        free((row->listeObj[i]).listePoids);
    }
    free(row->listeObj);
    free(row->poidMax);
    free(row);
}

/** fonction qui détruit l'instance prise en paramètre et désalloue tout la mémoire nécessaire
* @param instance * row : instance à détruire
*/
void instanceDB_destroy(instanceDB * row){
    for(int i = 0; i<row->nbInstance; i++){
        instance_destroy(row->listeInstance[i]);
    }
    free(row->listeInstance);
    free(row);
}

/** readInstance lit le contenu d'un fichier formaté et retourne une structure de type instance
*@param : FILE* fichier un pointeur sur un fichier formaté
*@return : instance * row : un pointeur sur une structure instance contenant :
* le nombre d'objet, le nombre de dimension, la liste d'objet, et leur poids
*/
instance * readInstance(FILE* fichier){
    instance * row = instance_create_and_init();

    //lecture fichier
    fscanf(fichier,"%d",&row->nbObjet);
    fscanf(fichier,"%d",&row->nbDimension);

    //sol1 et sol2 seront détruite on en a pas encore besoin
    fscanf(fichier,"%d",&row->solution1);
    fscanf(fichier,"%d",&row->solution2);

    // on passe les xj dans une variable locale qui sera détruite, pourra être remplacer par une fct read_xj
    row->sol = read_tab(fichier, row->nbObjet);

    //init liste objet + lecture de chaque objet
    row->listeObj = objet_read_valeur(fichier, row->nbObjet);
    objet_read_poids(fichier, row);

    //lit les poids max par dimension
    row->poidMax = read_tab(fichier, row->nbDimension);

    return row;


}
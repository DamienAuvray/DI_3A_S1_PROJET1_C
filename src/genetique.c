#include "../header/genetique.h"
#include "../header/solution.h"
#include "../header/instance.h"
#include "../header/heuristique.h"
#include "../header/matheuristique.h"
#include "../header/resolution.h"
#include "../header/tool.h"
#include "../header/ordonnancement.h"
#include "../header/decodeur.h"
#include <time.h>

/** genere une population contenant des solution directe aleatoire
*@param instance * row : instance en cours
*@param int taillePopu : la taille que l'on veut donner à la population (le nb de solution aleatoire)
*@return solution ** population : la population finale
*/
solution ** calculPopulation(instance * row, int taillePopu){
    /** creation population */
    solution ** population = (solution **)malloc((size_t)taillePopu*sizeof(solution*));

    /** pour chaque sol de population on appel la méthode 2 (solution aleatoire)*/
    for(int i = 0; i<taillePopu; i++)
        population[i]=heuristiqueDirecte(row,2);

    return population;
}

/** trouve la meilleure solution d'une population donnée
*@param solution ** popu : la population ou l'on cherche la meilleure solution
*@param instance * row : l'instance en cours
*@param int taillePopu : la taille de la population
*@return solution * sol : la meilleure solution de la population
*/
solution * meilleureSolution(solution ** popu, instance * row, int taillePopu){
    solution * sol = solution_create(row->nbObjet);

    int fMax = 0;
    int fCourant = 0;

    for (int i=0; i<taillePopu; i++){
        fCourant = fonctionObjective_directe(popu[i],row);
        if(fCourant>fMax){
            fMax = fCourant;
            solution_copy(popu[i],sol,row->nbObjet);
        }

    }

    return sol;
}

/** selectionne deux solution dans une population en fonction du pourcentage que représente leur fct objective
*@param solution * P1 : solution parent 1
*@param solution * P2 : solution parent 2
*@param solution ** popu : la population dans laquelle on veut selectionner des parents
*@param : int taillePopu : le nb de solution dans la population
*@param : instance * row : l'instance en cours
*/
void selectionParent(solution * P1, solution * P2, solution ** popu, int taillePopu, instance * row){
    int fTotal = 0;
    int fCourant = 0;
    double fTab[taillePopu];
    int alea = 0;
    int indiceRand = 0;

    /** on calcul le total de toute les fct obj de la population, et on creéer un tab de fct obj */
    for(int i = 0; i<taillePopu; i++){
        fTotal += fonctionObjective_directe(popu[i],row);
        fTab[i]= fonctionObjective_directe(popu[i],row);
    }
    int * roulette = (int *)malloc((size_t)fTotal*sizeof(int));
    /** on remprit la roulette en proportion de la fct obj de chaque solution de la population */
    for(int i = 0; i<taillePopu; i++){
        for(int j = fCourant; j<(fCourant+fTab[i]); j++)
            roulette[j]=i;
        fCourant += fTab[i];
    }
    /** definition d'un indice aleatoire dans la roulette */
    alea = rand()%fTotal;
    indiceRand = roulette[alea];
    /** copy de la solution trouvé dans le parent */
    solution_copy(popu[indiceRand],P1, row->nbObjet);
    do{
    alea = rand()%fTotal;
    }while(roulette[alea]==indiceRand);
    indiceRand = roulette[alea];
    solution_copy(popu[indiceRand],P2, row->nbObjet);
    free(roulette);

}

/** créer deux solution filles à partir de 2 parents
*@param solution * E1 : solution fille 1;
*@param solution * E2 : solution fille 2;
*@param solution * P1 : solution parent 1;
*@param solution * P2 : solution parent 2;
*@param instance * row : instance en cours;
*/
void creationEnfants(solution * E1, solution * E2, solution * P1, solution * P2, instance * row){
    /** definition aléatoire d'un point de coupure à partir duquel on va inverser P1 et P2 pour creer E1 et E2*/
    int pointCut = rand()%(row->nbObjet);
    /** init du debut des solution enfant avec leur parent respectifs */
    for(int i = 0; i<pointCut; i++){
        E1->tabBinaire[i] = P1->tabBinaire[i];
        E2->tabBinaire[i]= P2->tabBinaire[i];    }
    /** la fin de E1 devient P2 et la fin de E2 devient P1*/
    for(int i=pointCut; i<row->nbObjet; i++){
        E1->tabBinaire[i] = P2->tabBinaire[i];
        E2->tabBinaire[i]= P1->tabBinaire[i];
    }
    /** si E1 ou E2 n'est pas valide on le "répare" pour qu'il le soit */
    reparation(E1,row);
    reparation(E2,row);
    /** on optimise les enfants en ajoutant des objets aleatoirement si c'est possible */
    optimisationEnfant(E1,row);
    optimisationEnfant(E2,row);
}

/** répare une solution : si la solution n'est pas valide on fait en sorte qu'elle le soit en retirant aleatoirement des objets
*@param solution * sol : la solution à réparer
*@param instance * row : l'instance en cours
*/
void reparation(solution * sol, instance * row){
    int tab[row->nbObjet];
    int alea = 0;
    int indice = 0;
    int i = 0;
    /** création d'un tableau d'indice */
    for(int j = 0; j<row->nbObjet; j++)
        tab[j]=j;
    /** tant que la solution est invalide on retire des objet */
    while(test_solution_directe(sol,row)==0){
        /** selection d'un indice aleatoire */
        if(i<row->nbObjet)
            alea = rand()%((row->nbObjet)-i);
        else
            alea = tab[0];
        /** tab[alea] permet de ne jamais retomber sur le même indice */
        indice = tab[alea];
        /**décalege des élement de tab pour supprimer l'indice que l'on a choisi */
        for(int j = indice; j<row->nbObjet-1; j++)
            tab[j]=tab[j+1];
        /** si l'objet selectionné est dans le sac on le retire */
        if(sol->tabBinaire[indice]==1)
            sol->tabBinaire[indice]=0;

        i++;
    }
}

/** optimise une solution en ajoutant des objet de valeurs
*@param : solution * sol : la solution à optimiser
*@param : instance * row : l'instance en cours
*/
void optimisationEnfant(solution * sol, instance *row){
    /** defini le tableau des indice des objets en fonction de leurs valeurs décroissantes */
    int * tab = ordonnancement_valeur(row);

    /** pour chaque objet, par ordre de valeur :
    *on ajoute l'objet
    *on vérifie si la solution est faisable
    *si elle l'est pas on retire l'objet qu'on a ajouté
    */
    for(int i = 0; i<row->nbObjet; i++){
        if(sol->tabBinaire[tab[i]]==0){
            sol->tabBinaire[tab[i]]=1;
            if(test_solution_directe(sol,row)==0)
                sol->tabBinaire[tab[i]]=0;
        }

    }

    free(tab);

}

/** fait muter une solution
*@param : solution * sol : la solution à muter
*@param : instance * row : l'instance en cours
*/
void mutation(solution * sol, instance * row){
    /** selectionne un bit aleatoire dans le sac */
    int alea = rand()%(row->nbObjet);
    /** si objet selectionné dans le sac on le retire (1=0), si il n'est pas dans le sac on l'ajoute (0=1)*/
    sol->tabBinaire[alea] = !sol->tabBinaire[alea];
    /** si solution invalide on fait l'inverse */
    if(test_solution_directe(sol,row)==0)
        sol->tabBinaire[alea] = !sol->tabBinaire[alea];
}

/** renouvelle une population en selectionnant les meilleures solutions de l'ensemble populationEnfant + populationParent
*@param : solution ** popCourante : la population que l'on veut renouveller, (population parent)
*@param : solution ** popEnfant : la population enfant
*@param : instance * row : instance en cours
*@param : int taillePopu : la taille d'une population
*/
void renouvellement(solution** popCourante, solution ** popEnfant, instance * row, int taillePopu){
    /** definition de l'ensemble parents + enfants */
    int taille = 2*taillePopu;
    solution ** popTotale = population_create((taillePopu*2),row);
    for(int i = 0; i<taillePopu; i++){
        solution_copy(popCourante[i],popTotale[i],row->nbObjet);
        solution_copy(popEnfant[i],popTotale[i+taillePopu-1],row->nbObjet);
    }
    /** variable temporaire */
    solution * temp = solution_create(row->nbObjet);
    /** classement des solutions de la population totale par ordre Decroissant de fonction Objective */
    for(int i = 0; i<taille-1; i++){
        for(int j = i+1; j<taille; j++){
            if(fonctionObjective_directe(popTotale[j],row)>fonctionObjective_directe(popTotale[i],row)){
                solution_copy(popTotale[i],temp,row->nbObjet);
                solution_copy(popTotale[j],popTotale[i],row->nbObjet);
                solution_copy(temp,popTotale[j],row->nbObjet);
            }
        }
    }
    /** remplissage de la nouvelle population courante */
    for(int i = 0; i<taillePopu; i++)
        solution_copy(popTotale[i],popCourante[i], row->nbObjet);

    /** liberation mémoire */
    solution_destroy(temp);
    population_destroy(popTotale,(taillePopu*2));
}

/** Algorithme génétique codage directe, s'appuyant sur la théorie de darwin pour trouver la meilleure solution d'une instance d'objets
*@param instance * row : l'instance dans laquelle on veut trouver la meilleure solution
*@param int nbIteMax : le nombre de fois que l'on va renouveller une population (ensemble de solution aléatoire)
*@param int taillePopu : le nb de solution d'une population
*@param double pMut : la probabilité de mutation d'un enfant (solution fille)
*@return solution * solutionBest : la meilleure solution trouver par l'algorithme
*/
solution * algoGenetique_directe(instance * row, int nbIteMax, int taillePopu, double pMut){
    solution ** populationCourante = calculPopulation(row, taillePopu); /** calcul d'un ensemble de solutions aléatoires */
    solution ** populationEnfant = NULL;
    solution * solutionBest = meilleureSolution(populationCourante,row,taillePopu); /** meilleure solution de la population de début */
    int fBest = fonctionObjective_directe(solutionBest, row);
    int i = 0;
    int comptEnfant = 0;
    double mutationBoolean = 0.0;
    /** création des variables de selection de parents et création d'enfants */
    solution * P1 = solution_create(row->nbObjet);
    solution * P2 = solution_create(row->nbObjet);
    solution * E1 =  solution_create(row->nbObjet);
    solution * E2 = solution_create(row->nbObjet);
    /**création de la population enfant vide */
    populationEnfant = population_create(taillePopu,row);

    /** pour chaque renouvellement * on créer une population enfant à partir de la population courante
    * on regarde si il y a une solution meilleure que la bestSolution dans la population enfant
    * si oui on la copie
    * en fonction de pMut, on mute les solution enfants et on regarde si ça permet de trouver une bestSolution
    * enfin on renouvelle la population courante */
    while(i<nbIteMax){
        comptEnfant = 0;
        for(int j = 0; j<taillePopu/2; j++){
            /** selectionne deux parents dans la population courante */
            selectionParent(P1, P2, populationCourante, taillePopu, row);
            /** créer deux enfants à partir des parents selectionnés */
            creationEnfants(E1, E2, P1, P2,row);
            /** copie dans la population enfant */
            solution_copy(E1,populationEnfant[comptEnfant],row->nbObjet);
            solution_copy(E2,populationEnfant[comptEnfant+1],row->nbObjet);
            comptEnfant += 2;
        }

        /** pour chaque enfant on regarde si il est meilleure que la bestSolution, et si besoin on le mute */
        for(int j = 0; j<comptEnfant; j++){
            if(fonctionObjective_directe(populationEnfant[j],row)>fBest){
                fBest = fonctionObjective_directe(populationEnfant[j],row);
                solution_copy(populationEnfant[j],solutionBest, row->nbObjet);
            }
            mutationBoolean = rand()%100;
            if(mutationBoolean/100 <= pMut){
                mutation(populationEnfant[j],row);
                if(fonctionObjective_directe(populationEnfant[j], row)>fBest){
                    fBest = fonctionObjective_directe(populationEnfant[j],row);
                    solution_copy(populationEnfant[j],solutionBest, row->nbObjet);
                }


            }
        }
        /** on renouvelle la population en prenant les meilleure des enfants+parents */
        renouvellement(populationCourante, populationEnfant, row, taillePopu);
        i++;

    }
    /** liberation mémoire */
    solution_destroy(P1);
    solution_destroy(P2);
    solution_destroy(E1);
    solution_destroy(E2);
    population_destroy(populationCourante,taillePopu);
    population_destroy(populationEnfant,taillePopu);
    return solutionBest;

}

/** genere une population et un ensemble de permutation contenant respectivement des solution indirecte et permutation aleatoire
*@param int ** permutation : un tableau de permutation que l'on va remplir aléatoirement
*@param solutionIndirecte ** popu : une population que l'on va remplir de solution aléatoire
*@param instance * row : instance en cours
*@param int taillePopu : la taille que l'on veut donner à la population (le nb de solution aleatoire)
*/
void calculPopulation_indirecte(int ** permutation, solutionIndirecte ** popu, instance * row, int taillePopu){

    int * permuTemp = NULL;
    solutionIndirecte * solTemp = NULL;

    for(int i = 0; i<taillePopu; i++){
        /** defini une permu grace à un ordonnancement d'indice d'objet aléatoire */
        permuTemp = ordonnancement_alea(row);
        /** créer une solution à partir d'une permutation aléatoire créée */
        solTemp = decoder(permuTemp,row);
        permutation_copy(permuTemp,permutation[i],row->nbObjet);
        solution_indirecte_copy(solTemp, popu[i]);
        free(permuTemp);
        solutionIndirecte_destroy(solTemp);
    }
}

/** trouve la meilleure solution d'une population donnée
*@param solutionIndirecte ** popu : la population ou l'on cherche la meilleure solution
*@param instance * row : l'instance en cours
*@param int taillePopu : la taille de la population
*@return solutionIndirecte * sol : la meilleure solution de la population
*/
solutionIndirecte * meilleureSolution_indirecte(solutionIndirecte ** popu, instance * row, int taillePopu){
    solutionIndirecte * sol = solutionIndirecte_create(0);

    int fMax = 0;
    int fCourant = 0;

    for (int i=0; i<taillePopu; i++){
        fCourant = fonctionObjective_indirecte(popu[i],row);
        if(fCourant>fMax){
            fMax = fCourant;
            solution_indirecte_copy(popu[i],sol);
        }

    }

    return sol;
}

/** selectionne deux permutation dans un ensemble de permutations en fonction du pourcentage que représente les fct objectives des solutions
*@param int * P1 : permutation parent 1
*@param int * P2 : permutation parent 2
*@param solutionIndirecte ** popu : la population dans laquelle on classe les fct objectives
*@param int ** permu : ensemble de permutation dans lequel on veut faire la sélection
*@param : int taillePopu : le nb de solution dans la population
*@param : instance * row : l'instance en cours
*/
void selectionParent_indirecte(int * P1, int * P2, solutionIndirecte ** popu, int**permu, int taillePopu, instance * row){
    int fTotal = 0;
    int fCourant = 0;
    double fTab[taillePopu];
    int alea = 0;
    int indiceRand = 0;

    /** on calcul le total de toute les fct obj de la population, et on creéer un tab de fct obj */
    for(int i = 0; i<taillePopu; i++){
        fTotal += fonctionObjective_indirecte(popu[i],row);
        fTab[i]= fonctionObjective_indirecte(popu[i],row);
    }
    int * roulette = (int *)malloc((size_t)fTotal*sizeof(int));
    /** on remprit la roulette en proportion de la fct obj de chaque solution de la population */
    for(int i = 0; i<taillePopu; i++){
        for(int j = fCourant; j<(fCourant+fTab[i]); j++)
            roulette[j]=i;
        fCourant += fTab[i];
    }
    /** selection d'un indice aléatoire */
    alea = rand()%fTotal;
    indiceRand = roulette[alea];
    permutation_copy(permu[indiceRand],P1,row->nbObjet);
    do{
    alea = rand()%fTotal;
    }while(roulette[alea]==indiceRand);
    indiceRand = roulette[alea];
    permutation_copy(permu[indiceRand],P2,row->nbObjet);
    free(roulette);

}

/** créer deux permutations filles à partir de 2 parents
*@param int * E1 : permutation fille 1;
*@param int * E2 : permutation fille 2;
*@param int * P1 : permutation parent 1;
*@param int * P2 : permutation parent 2;
*@param instance * row : instance en cours;
*/
void creationEnfants_indirecte(int * E1, int * E2, int * P1, int * P2, instance * row){
    /** définition de deux point de coupures */
    int pointCut1 = rand()%(row->nbObjet/2);
    int pointCut2 = rand()%(row->nbObjet - row->nbObjet/2) + (row->nbObjet/2);
    int echange = 0;
    int indice = 0;

    /** on inverse les elements entre les deux pt de coupure */
    for(int i=pointCut1; i<pointCut2; i++){
        E1[i] = P2[i];
        E2[i]= P1[i];
    }

    /** le debut des permutations filles */
    for(int i = 0; i<pointCut1; i++){
        echange = P1[i];
        while(trouverElement(P2,echange,pointCut1,pointCut2)==1){
            indice = trouverIndice(P2,echange,pointCut1,pointCut2);
            echange = P1[indice];
        }
        E1[i]=echange;
    }
    for(int i=0; i<pointCut1; i++){
        echange = P2[i];
        while(trouverElement(P1,echange,pointCut1,pointCut2)==1){
            indice = trouverIndice(P1,echange,pointCut1,pointCut2);
            echange = P2[indice];
        }
        E2[i]=echange;
    }


    /** pour la fin on fait référence à une table d'échange */
    for(int i=pointCut2; i<row->nbObjet; i++){
        echange = P1[i];
        while(trouverElement(P2,echange,pointCut1,pointCut2)==1){
            indice = trouverIndice(P2,echange,pointCut1,pointCut2);
            echange = P1[indice];
        }
        E1[i]=echange;
    }
    for(int i=pointCut2; i<row->nbObjet; i++){
        echange = P2[i];
        while(trouverElement(P1,echange,pointCut1,pointCut2)==1){
            indice = trouverIndice(P1,echange,pointCut1,pointCut2);
            echange = P2[indice];
        }
        E2[i]=echange;
    }

}

/** fait muter une solution
*@param int * permu : permutation correspondante à la solution à muter
*@param : solutionIndirecte * sol : la solution à muter
*@param : instance * row : l'instance en cours
*/
void mutation_indirecte(int * permu, solutionIndirecte * sol, instance * row){
    solutionIndirecte * solTemp = NULL;
    /** definition de deux indices aleatoires */
    int alea1 = rand()%(row->nbObjet);
    int alea2 = 0;
    int temp = 0;

    do{
        alea2 = rand()%(row->nbObjet);
    }while(alea2==alea1);

    /** echange des deux objets selectionnés dans la permutation */
    temp = permu[alea1];
    permu[alea1]=permu[alea2];
    permu[alea2]=temp;

    /** création de la solution mutée à partir de la permutation */
    solTemp = decoder(permu,row);

    /** copie dans solution fille que l'on voulait muter */
    solution_indirecte_copy(solTemp,sol);

    solutionIndirecte_destroy(solTemp);
}

/** renouvelle une population en selectionnant les meilleures solutions de l'ensemble populationEnfant + populationParent
*@param : int **permuCourante : l'ensemble de permutation parent que l'on veut renouveller
*@param : int ** permuEnfant : l'ensemble de permutations enfants
*@param : solutionIndirecte ** popCourante : la population que l'on veut renouveller, (population parent)
*@param : solutionIndirecte ** popEnfant : la population enfant
*@param : instance * row : instance en cours
*@param : int taillePopu : la taille d'une population
*/
void renouvellement_indirecte(int **permuCourante, int** permuEnfant, solutionIndirecte** popCourante, solutionIndirecte ** popEnfant, instance * row, int taillePopu){
    /** definition de l'ensemble pop enfant + pop parent et l'ensemble permu parent + permu enfant */
    int taille = 2*taillePopu;
    solutionIndirecte ** popTotale = population_indirecte_create(taille,row);
    int ** permuTotale = permution_tab_create(row,taille);
    solutionIndirecte * temp = solutionIndirecte_create(0);
    int * permuTemp = permutation_create(row);

    for(int i = 0; i<taillePopu; i++){
        solution_indirecte_copy(popCourante[i],popTotale[i]);
        permutation_copy(permuCourante[i], permuTotale[i],row->nbObjet);
    }
    for(int i = taillePopu;i<taille;i++){
        solution_indirecte_copy(popEnfant[i-taillePopu],popTotale[i]);
        permutation_copy(permuEnfant[i-taillePopu], permuTotale[i],row->nbObjet);
    }

    /** clasemment de permut et solutions en fonctions des foncions objectives des solutions décroissantes */
    for(int i = 0; i<taille; i++){
        for(int j = i+1; j<taille; j++){
            if(fonctionObjective_indirecte(popTotale[j],row)>fonctionObjective_indirecte(popTotale[i],row)){
                solution_indirecte_copy(popTotale[i],temp);
                permutation_copy(permuTotale[i],permuTemp,row->nbObjet);
                solution_indirecte_copy(popTotale[j],popTotale[i]);
                permutation_copy(permuTotale[j],permuTotale[i],row->nbObjet);
                solution_indirecte_copy(temp,popTotale[j]);
                permutation_copy(permuTemp,permuTotale[j],row->nbObjet);
            }
        }
    }
    /** copie des "taillePopu" meilleures dans les pop et permut courantes */
    for(int i = 0; i<taillePopu; i++){
        solution_indirecte_copy(popTotale[i],popCourante[i]);
        permutation_copy(permuTotale[i],permuCourante[i],row->nbObjet);
    }
    /** libération mémoire */
    population_indirecte_destroy(popTotale,taille);
    permutation_tab_destroy(permuTotale,taille);
    solutionIndirecte_destroy(temp);
    free(permuTemp);

}

/** Algorithme génétique codage indirecte, s'appuyant sur la théorie de darwin pour trouver la meilleure solution d'une instance d'objets
*@param instance * row : l'instance dans laquelle on veut trouver la meilleure solution
*@param int nbIteMax : le nombre de fois que l'on va renouveller une population (ensemble de solution aléatoire)
*@param int taillePopu : le nb de solution d'une population
*@param double pMut : la probabilité de mutation d'un enfant (solution fille)
*@return solutionIndirecte * solutionBest : la meilleure solution trouver par l'algorithme
*/
solutionIndirecte * algoGenetique_indirecte(instance * row, int nbIteMax, int taillePopu, double pMut){
    /** definition des population et ensemble de permut courante */
    solutionIndirecte ** populationCourante = population_indirecte_create(taillePopu,row);
    int ** permutationCourante = permution_tab_create(row,taillePopu);
    /** calcul d'une population et d'un ensemble de permut correspondantes de façon aléatoire */
    calculPopulation_indirecte(permutationCourante, populationCourante, row, taillePopu);
    /**définition population et ensemble de permut enfant */
    solutionIndirecte ** populationEnfant = population_indirecte_create(taillePopu,row);
    int ** permutationEnfant = permution_tab_create(row,taillePopu);

    solutionIndirecte * solutionBest = meilleureSolution_indirecte(populationCourante,row,taillePopu);
    int fBest = fonctionObjective_indirecte(solutionBest,row);
    int i = 0;
    int comptEnfant = 0;
    double probaAlea = 0.0;
    /** variables qui serviront à sélectionner les parents et créer les enfants */
    int * P1 = permutation_create(row);
    int * P2 = permutation_create(row);
    int * E1 =  permutation_create(row);
    int * E2 = permutation_create(row);
    solutionIndirecte * S1 = NULL;
    solutionIndirecte * S2 = NULL;


    while(i<nbIteMax){

        comptEnfant = 0;
        /** selection de deux permut parents et créations de deux enfants avec */
        for(int j = 0; j<taillePopu/2; j++){
            selectionParent_indirecte(P1, P2, populationCourante, permutationCourante, taillePopu, row);
            creationEnfants_indirecte(E1, E2, P1, P2,row);
            permutation_copy(E1,permutationEnfant[comptEnfant],row->nbObjet);
            permutation_copy(E2,permutationEnfant[comptEnfant+1],row->nbObjet);
            S1 = decoder(E1,row);
            S2 = decoder(E2,row);
            solution_indirecte_copy(S1,populationEnfant[comptEnfant]);
            solution_indirecte_copy(S2,populationEnfant[comptEnfant+1]);
            solutionIndirecte_destroy(S1);
            solutionIndirecte_destroy(S2);
            comptEnfant += 2;
        }
        /** pour chaque enfants : on vérifie si il y en a un meilleure que la bestSolution
        * on le mute en fonction d'une probabilité de mutation
        * on reverifie si il est meilleur que la meilleure des solutions */
        for(int j = 0; j<comptEnfant; j++){
            if(fonctionObjective_indirecte(populationEnfant[j],row)>fBest){
                fBest = fonctionObjective_indirecte(populationEnfant[j],row);
                solution_indirecte_copy(populationEnfant[j],solutionBest);
            }
            probaAlea = rand()%100;
            if(probaAlea/100 <= pMut){
                mutation_indirecte(permutationEnfant[j], populationEnfant[j], row);
                if(fonctionObjective_indirecte(populationEnfant[j], row)>fBest){
                    fBest = fonctionObjective_indirecte(populationEnfant[j],row);
                    solution_indirecte_copy(populationEnfant[j],solutionBest);
                }
            }
        }
        /** on renouvelle la population courante en selectionnant les meilleurs des parents et enfants réunis */
        renouvellement_indirecte(permutationCourante, permutationEnfant, populationCourante, populationEnfant, row, taillePopu);
        i++;
    }
    /**libération mémoire */
    population_indirecte_destroy(populationCourante,taillePopu);
    population_indirecte_destroy(populationEnfant,taillePopu);
    permutation_tab_destroy(permutationCourante, taillePopu);
    permutation_tab_destroy(permutationEnfant,taillePopu);
    free(P1);
    free(P2);
    free(E1);
    free(E2);

    return solutionBest;
}



/** Algorithme génétique codage directe couplé à la recherche locale lors de la mutation
*@param instance * row : l'instance dans laquelle on veut trouver la meilleure solution
*@param int nbIteMax : le nombre de fois que l'on va renouveller une population (ensemble de solution aléatoire)
*@param int taillePopu : le nb de solution d'une population
*@param double pMut : la probabilité de mutation d'un enfant (solution fille)
*@return solution * solutionBest : la meilleure solution trouver par l'algorithme
*/
solution * algoGenetique_directe_rechercheLocale(instance * row, int nbIteMax, int taillePopu, double pMut){
    solution ** populationCourante = calculPopulation(row, taillePopu); /** calcul d'un ensemble de solutions aléatoires */
    solution ** populationEnfant = NULL;
    solution * solutionBest = meilleureSolution(populationCourante,row,taillePopu); /** meilleure solution de la population de début */
    int fBest = fonctionObjective_directe(solutionBest, row);
    int i = 0;
    int comptEnfant = 0;
    double mutationBoolean = 0.0;
    /** création des variables de selection de parents et création d'enfants */
    solution * P1 = solution_create(row->nbObjet);
    solution * P2 = solution_create(row->nbObjet);
    solution * E1 =  solution_create(row->nbObjet);
    solution * E2 = solution_create(row->nbObjet);
    /**création de la population enfant vide */
    populationEnfant = population_create(taillePopu,row);

    /** pour chaque renouvellement * on créer une population enfant à partir de la population courante
    * on regarde si il y a une solution meilleure que la bestSolution dans la population enfant
    * si oui on la copie
    * en fonction de pMut, on mute les solution enfants et on regarde si ça permet de trouver une bestSolution
    * enfin on renouvelle la population courante */
    while(i<nbIteMax){
        comptEnfant = 0;
        for(int j = 0; j<taillePopu/2; j++){
            /** selectionne deux parents dans la population courante */
            selectionParent(P1, P2, populationCourante, taillePopu, row);
            /** créer deux enfants à partir des parents selectionnés */
            creationEnfants(E1, E2, P1, P2,row);
            /** copie dans la population enfant */
            solution_copy(E1,populationEnfant[comptEnfant],row->nbObjet);
            solution_copy(E2,populationEnfant[comptEnfant+1],row->nbObjet);
            comptEnfant += 2;
        }

        /** pour chaque enfant on regarde si il est meilleure que la bestSolution, et si besoin on le mute */
        for(int j = 0; j<comptEnfant; j++){
            if(fonctionObjective_directe(populationEnfant[j],row)>fBest){
                fBest = fonctionObjective_directe(populationEnfant[j],row);
                solution_copy(populationEnfant[j],solutionBest, row->nbObjet);
            }
            mutationBoolean = rand()%100;
            if(mutationBoolean/100 <= pMut){
                rechercheLocale_directe_Invention(populationEnfant[j],row);
                if(fonctionObjective_directe(populationEnfant[j], row)>fBest){
                    fBest = fonctionObjective_directe(populationEnfant[j],row);
                    solution_copy(populationEnfant[j],solutionBest, row->nbObjet);
                }


            }
        }
        /** on renouvelle la population en prenant les meilleure des enfants+parents */
        renouvellement(populationCourante, populationEnfant, row, taillePopu);
        i++;

    }
    /** liberation mémoire */
    solution_destroy(P1);
    solution_destroy(P2);
    solution_destroy(E1);
    solution_destroy(E2);
    population_destroy(populationCourante,taillePopu);
    population_destroy(populationEnfant,taillePopu);
    return solutionBest;

}

/** Algorithme génétique codage indirecte couplé à la recherche locale lors de la mutation
*@param instance * row : l'instance dans laquelle on veut trouver la meilleure solution
*@param int nbIteMax : le nombre de fois que l'on va renouveller une population (ensemble de solution aléatoire)
*@param int taillePopu : le nb de solution d'une population
*@param double pMut : la probabilité de mutation d'un enfant (solution fille)
*@return solutionIndirecte * solutionBest : la meilleure solution trouver par l'algorithme
*/
solutionIndirecte * algoGenetique_indirecte_rechercheLocale(instance * row, int nbIteMax, int taillePopu, double pMut){
    /** definition des population et ensemble de permut courante */
    solutionIndirecte ** populationCourante = population_indirecte_create(taillePopu,row);
    int ** permutationCourante = permution_tab_create(row,taillePopu);
    /** calcul d'une population et d'un ensemble de permut correspondantes de façon aléatoire */
    calculPopulation_indirecte(permutationCourante, populationCourante, row, taillePopu);
    /**définition population et ensemble de permut enfant */
    solutionIndirecte ** populationEnfant = population_indirecte_create(taillePopu,row);
    int ** permutationEnfant = permution_tab_create(row,taillePopu);

    solutionIndirecte * solutionBest = meilleureSolution_indirecte(populationCourante,row,taillePopu);
    int fBest = fonctionObjective_indirecte(solutionBest,row);
    int i = 0;
    int comptEnfant = 0;
    double probaAlea = 0.0;
    /** variables qui serviront à sélectionner les parents et créer les enfants */
    int * P1 = permutation_create(row);
    int * P2 = permutation_create(row);
    int * E1 =  permutation_create(row);
    int * E2 = permutation_create(row);
    solutionIndirecte * S1 = NULL;
    solutionIndirecte * S2 = NULL;


    while(i<nbIteMax){

        comptEnfant = 0;
        /** selection de deux permut parents et créations de deux enfants avec */
        for(int j = 0; j<taillePopu/2; j++){
            selectionParent_indirecte(P1, P2, populationCourante, permutationCourante, taillePopu, row);
            creationEnfants_indirecte(E1, E2, P1, P2,row);
            permutation_copy(E1,permutationEnfant[comptEnfant],row->nbObjet);
            permutation_copy(E2,permutationEnfant[comptEnfant+1],row->nbObjet);
            S1 = decoder(E1,row);
            S2 = decoder(E2,row);
            solution_indirecte_copy(S1,populationEnfant[comptEnfant]);
            solution_indirecte_copy(S2,populationEnfant[comptEnfant+1]);
            solutionIndirecte_destroy(S1);
            solutionIndirecte_destroy(S2);
            comptEnfant += 2;
        }
        /** pour chaque enfants : on vérifie si il y en a un meilleure que la bestSolution
        * on le mute en fonction d'une probabilité de mutation
        * on reverifie si il est meilleur que la meilleure des solutions */
        for(int j = 0; j<comptEnfant; j++){
            if(fonctionObjective_indirecte(populationEnfant[j],row)>fBest){
                fBest = fonctionObjective_indirecte(populationEnfant[j],row);
                solution_indirecte_copy(populationEnfant[j],solutionBest);
            }
            probaAlea = rand()%100;
            if(probaAlea/100 <= pMut){
                rechercheLocale_indirecte_invention(permutationEnfant[j], populationEnfant[j], row);
                if(fonctionObjective_indirecte(populationEnfant[j], row)>fBest){
                    fBest = fonctionObjective_indirecte(populationEnfant[j],row);
                    solution_indirecte_copy(populationEnfant[j],solutionBest);
                }
            }
        }
        /** on renouvelle la population courante en selectionnant les meilleurs des parents et enfants réunis */
        renouvellement_indirecte(permutationCourante, permutationEnfant, populationCourante, populationEnfant, row, taillePopu);
        i++;
    }
    /**libération mémoire */
    population_indirecte_destroy(populationCourante,taillePopu);
    population_indirecte_destroy(populationEnfant,taillePopu);
    permutation_tab_destroy(permutationCourante, taillePopu);
    permutation_tab_destroy(permutationEnfant,taillePopu);
    free(P1);
    free(P2);
    free(E1);
    free(E2);

    return solutionBest;
}

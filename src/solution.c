/************************************************************
* ensemble de fonctions relative à la création, destruction et
*manipulation de solution directe et indirecte
************************************************************/
#include "../header/solution.h"
#include "../header/instance.h"

/****************************
*CODAGE DIRECTE
****************************/

/** créer une solution directe et l'initialise
*@param : int nbObjet : le nombre d'objet de l'instance
*@return solution * sol : la solution initialisée
*/
solution * solution_create(int nbObjet){
    solution * sol = (solution*)malloc(sizeof(solution));
    sol->tabBinaire = (int *)malloc((size_t)(nbObjet)*sizeof(int));
    for(int i = 0; i<nbObjet; i++)
        sol->tabBinaire[i] = 0;
    return sol;
}

/** détruit et désalloue l'espace mémoire pris par une solution allouée sur le tas
*@param solution * sol : la solution à détruire
*/
void solution_destroy(solution * sol){
    if(sol->tabBinaire != NULL) free(sol->tabBinaire);
    if(sol != NULL) free(sol);
}

/** copy une solution directe s1 dans une solution directe s2
*@param : sol1 la solution à copier
*@param : sol2 la solution résultat
*@param : nbObjet : le nb d'obj de l'instance
*/
void solution_copy(solution * sol1, solution * sol2, int nbObjet){
    for(int i = 0; i<nbObjet; i++)
        sol2->tabBinaire[i]= sol1->tabBinaire[i];
}

/** créer une population, un ensemble de solution directe
*@param int taillePopu : la taille de notre population, le nb de solutions
*@param instance * row : l'instance en cours
*/
solution ** population_create(int taillePopu, instance * row){
    solution ** popu = (solution**)malloc((size_t)taillePopu*sizeof(solution*));
    for(int i = 0; i<taillePopu; i++)
        popu[i] = solution_create(row->nbObjet);
    return popu;
}

/** détruit et désalloue l'espace mémoire pris par une population allouée sur le tas
*@param solution ** : la population à détruire
*@param int taillePopu : la taille de la population
*/
void population_destroy(solution ** popu, int taillePopu){
    for(int i = 0; i<taillePopu; i++)
        solution_destroy(popu[i]);
    if(popu!=NULL) free(popu);
}

/** affiche dans la console une solution en fonction d'une solution directe
*@param : solution * sol : la solution à afficher
*@param : instance * row : l'instance à laquelle se réfère la solution
*/
void afficher_directe(solution *sol, instance *row){
    for(int i=0;i<row->nbObjet;i++){
        if(sol->tabBinaire[i]==1)
            printf("\t%d",i);
    }
}

/** calcule la fonction objective d'une solution directe, (somme des valeurs des obj présent dans le sac)
*@param solution * sol : la solution pour laquelle on souhaite calculer la fonction objective
*@param instance * row : l'instance associée à la solution
*@return int valeur : la fonction objective de la soltuion
*/
int fonctionObjective_directe(solution *sol, instance *row){
    int  valeur= 0 ;
    for(int i=0;i<row->nbObjet;i++){
            if(sol->tabBinaire[i]==1)
                valeur += row->listeObj[i].valeur;
    }
    return valeur;
}

/** test si une solution directe est possible vis à vis des poid max de l'instance
*@param : solution * sol : la solution à tester
*@param : instance * row : l'instance associée à la solution
*@note : retourne 1 si la solution est possible et 0 sinon.
*/
int test_solution_directe(solution *sol, instance *row){
    int sommePoid = 0;
    /** pour chaque dimension on regarde si la somme des poid des objet pour cette dimension ne dépasse pas la capacité max */
    for(int i=0;i<row->nbDimension;i++){
        sommePoid = 0;
        for(int j = 0; j<row->nbObjet; j++){
            if(sol->tabBinaire[j]==1)
                sommePoid += row->listeObj[j].listePoids[i];
        }
        if(sommePoid > row->poidMax[i])
            return 0;
    }

    return 1;
}


/*****************************
* CODAGE INDIRECTE
*****************************/

/** créer une permutation allouer sur le tas et l'initialise
* @param instance * row : l'instance en cours
*/
int * permutation_create(instance * row){
    int * permu = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permu,row->nbObjet);
    return permu;
}

/** initialise une parmutation
*@param int * perm : la permutation à initialiser
*@param int nbObjet : le nombre d'objet dans la permutation
*/
void permutation_init(int * perm, int nbObjet){
    for(int i = 0; i<nbObjet; i++)
        perm[i]= i;
}

/** copie les valeurs d'une permutation 1 dans une permutation 2
*@param : int * perm1 : la permutation à copier
*@param : int * perm2 : la permutation dans laquelle on copie perm1
*@param : int nbObjet : le nombre d'objet dans les permutations
*/
void permutation_copy(int * perm1, int * perm2, int nbObjet){
    for(int i = 0; i<nbObjet; i++)
        perm2[i]=perm1[i];
}

/** créer un ensemble de permutation
*@param instance * row : l'instance en cours
*@param int taille : le nombre de permutation que l'on veut dans notre tableau
*@return int ** tab : le tableau de permutation alloué sur le tas
*/
int ** permution_tab_create(instance * row, int taille){
    int ** tab = (int **)malloc((size_t)taille*sizeof(int*));
    for(int i = 0; i<taille; i++)
        tab[i]=permutation_create(row);
    return tab;
}

/** détruit et désalloue tout l'espace mémoire pris par un ensemble de permutation
* int ** permu : le tableau de permutations  détruire
* int taillePopu : le nombre de permutation dans le tableau
*/
void permutation_tab_destroy(int ** permu, int taillePopu){
    for(int i = 0; i<taillePopu; i++)
        if(permu[i] != NULL) free(permu[i]);
    if(permu != NULL) free(permu);
}

/** créer une solution indirecte et l'initialise
*@param : int nbObjet : le nombre d'objet de la solution
*@return solutionIndirecte * sol : la solution initialisée
*/
solutionIndirecte * solutionIndirecte_create(int nbObjet){
    solutionIndirecte * sol = (solutionIndirecte*)malloc(sizeof(solutionIndirecte));
    sol->nbObjet = nbObjet;
    if(nbObjet == 0)
        sol->solution = NULL;
    else{
        sol->solution = (int *)malloc((size_t)nbObjet*sizeof(int));

        for(int i = 0; i<nbObjet; i++)
            sol->solution[i] = 0;
    }
    return sol;
}

/** désalloue l'ensemble de la mémoire utilisée par une solution indirecte
*@param solutionIndirecte * sol : la solution à détruire
*/
void solutionIndirecte_destroy(solutionIndirecte* sol){
    if(sol->solution != NULL) free(sol->solution);
    if(sol != NULL) free(sol);

}

/** copy une solution indirecte sol1 dans une solution indirecte sol2
* adapte la taille de la solution en fonction de la solution à copier
*@param : sol1 la solution à copier
*@param : sol2 la solution résultat
*/
void solution_indirecte_copy(solutionIndirecte * sol1, solutionIndirecte * sol2){
    sol2->nbObjet = sol1->nbObjet;
    if(sol2->solution!=NULL) free(sol2->solution);
    sol2->solution = (int *)malloc((size_t)(sol2->nbObjet)*sizeof(int));
    for(int i = 0; i<sol2->nbObjet; i++)
        sol2->solution[i]=sol1->solution[i];
}

/** créer et initialise une population, ensemble de solution indirecte
* @param int taillePopu : le nombre de solutions dans notre population
* @param instance * row : l'instance en cours
* @return solutionIndirecte ** popu : la population allouée sur le tas
*/
solutionIndirecte ** population_indirecte_create(int taillePopu, instance * row){
    solutionIndirecte ** popu = (solutionIndirecte**)malloc((size_t)(taillePopu)*sizeof(solutionIndirecte*));
    for(int i = 0; i<taillePopu; i++)
        popu[i] = solutionIndirecte_create(0);
    return popu;
}

/** détruit et désalloue toute la mémoire utilisée par une population de solutions
*@param solutionIndirecte ** popu : la population à détruire
*@param int taillePopu : la taille de la population à détruire
*/
void population_indirecte_destroy(solutionIndirecte ** popu, int taillePopu){
    for(int i = 0; i<taillePopu; i++)
        solutionIndirecte_destroy(popu[i]);
    if(popu != NULL) free(popu);
}



/** affiche dans la console une solution en fonction d'une solution indirecte
*@param : solutionIndirecte * sol : la solution à afficher
*/
void afficher_indirecte(solutionIndirecte *sol){
    for(int i=0;i<sol->nbObjet;i++){
        printf("\t%d",sol->solution[i]);
    }
}

/** calcule la fonction objective d'une solution indirecte, (somme des valeurs des obj présent dans le sac)
*@param solutionIndirecte * sol : la solution pour laquelle on souhaite calculer la fonction objective
*@param instance * row : l'instance associée à la solution
*@return int valeur : la fonction objective de la soltuion
*/
int fonctionObjective_indirecte(solutionIndirecte * sol, instance * row){
    int valeur = 0;
    for(int i = 0; i<sol->nbObjet; i++)
        valeur += (row->listeObj[sol->solution[i]]).valeur;
    return valeur;

}


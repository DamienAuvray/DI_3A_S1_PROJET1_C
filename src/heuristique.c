#include <time.h>
#include "../header/heuristique.h"
#include "../header/solution.h"
#include "../header/tool.h"
#include "../header/decodeur.h"
#include "../header/ordonnancement.h"


/** fonction qui trouve une solution directe d'une instance en fonction d'une méthode d'ordonnancement.
* @param instance * row : l'instance pour laquelle on cherche une solution.
* @param int numMethode : le numero de la methode entrer en paramètre du main
* @return solution * sol : la solution de l'instance
*/
solution * heuristiqueDirecte(instance * row, int numMethode){
	int * list = NULL;

	int i = 0;
    int numObjet = 0;
    int nbObjet = row->nbObjet;

    /** déclaration et initialisation d'une solution directe */
    solution * sol = solution_create(nbObjet);

    do{
        for(int i = 0; i<nbObjet; i++)
            sol->tabBinaire[i]=0;

        /** init cap max par dimension */
        int * capMax = (int *)malloc((size_t)(row->nbDimension)*sizeof(int));
        for(int k=0; k<row->nbDimension; k++)
            capMax[k]=row->poidMax[k];

        /** on créer une liste d'indice d'objet avec un certain ordonnancement
        * l'ordonnancement est choisi selon la méthode utlisié en paramètre
        */
        switch(numMethode){
            case 1 :
                list = ordonnancement_valeur(row);
            break;
            case 2 :
                list = ordonnancement_alea(row);
            break;
            case 3 :
                list = ordonnancement_ratio_valeurObjet_sommePoids(row);
            break;
            case 4 :
                list = ordonnancement_ratio_valeurObjet_dimensionCritique(row);
            break;
            case 5 :
                list = ordonnancement_invention(row);
            break;
        }

        /** pour chaque objet on regarde si on peut l'ajouter dans le sac et on diminue les capcité si c'est possible */
        while(i < nbObjet){
            numObjet = list[i];
            /** test si l'jout est possible */
            if(ajoutSacPossible(row, numObjet, capMax)==1){
                sol->tabBinaire[numObjet] = 1;

                /** on diminue les capacité restante pour chaque dimension dans le sac */
                for(int x=0; x<row->nbDimension; x++)
                capMax[x] = capMax[x]-(row->listeObj[numObjet].listePoids[x]);
            }
            i++;
        }

        free(list);
        free(capMax);
    }while(test_solution_directe(sol,row)==0);
    return sol;
}

/** fonction qui trouve une solution directe d'une instance.
* on ordonne une permutation en triant par ordre decroissant
* de la valeur d'un objet par rapport au poid de la dimension critique
* on modifie dynamiquement cette permutation en fonction de la section critique
* @param instance * row : l'instance pour laquelle on cherche une solution.
* @param int numMethode : le numero de la methode entrer en paramètre du main
* @return solution * sol : la solution de l'instance
*/
solution * heuristiqueDirecteDynamique(instance * row){
    /** on créer une première liste ordonner selon le ratio entre la valeur des objet et la dimension critique */
    int * list = ordonnancement_ratio_valeurObjet_dimensionCritique(row);
    int i = 0;
    int numObjet = 0;
    int nbObjet = row->nbObjet;

    /** init cap max par dimension */
    int * capMax = (int *)malloc((size_t)(row->nbDimension)*sizeof(int));
    for(int k=0; k<row->nbDimension; k++)
        capMax[k]=row->poidMax[k];

    /** déclaration et initialisation d'une solution directe */
    solution * sol = solution_create(nbObjet);

    /** pour cahque objet on regarde si l'ajout est possible
    * si on peut l'ajouter on peut alors diminuer la capacité du sac
    * on peut recalculer la nouvelle dimension critique
    */
    while(i < nbObjet){
        numObjet = list[0];

        /** test si l'ajout est possible */
        if(ajoutSacPossible(row, numObjet, capMax)==1){
            sol->tabBinaire[numObjet] = 1;

            /** on diminue les capacité restante pour chaque dimension dans le sac */
            for(int x=0; x<row->nbDimension; x++)
                capMax[x] = capMax[x]-(row->listeObj[numObjet].listePoids[x]);
            /** on modifie la list en fonction de la nouvelle dimension critique */
            list = updateDynamique(row, list, capMax, nbObjet-i);
        }
        i++;


    }

    free(list);
    free(capMax);
    return sol;
}


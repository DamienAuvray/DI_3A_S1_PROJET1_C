#include "../header/ordonnancement.h"
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../header/tool.h"
#include "../header/heuristique.h"
#include <time.h>


/** fonction qui retourne un tableaux d'indice dans l'ordre decroissant de la valeur correspondant à cette indice
*@param : int nbObjet : le nombre d'objet dans le tableaux à trier
*@param : double * tabValeur : un tableaux de valeur pouvant contenir des poid ou des ratio que l'on veut ordonner
*@return : int * result : le tableaux d'indice
* exemple : tab = {25,30,10,50}, result = {3,1,0,2} car 50>30>25>10
*/
int * ordonnancement(int nbObjet, double * tabValeur){
    /** variables locales */
    int i, j;
    double tempValeur;
    int tempNumero;
    /** tableau qui contient le numéro des objets */
    int * result = (int *)malloc((size_t)(nbObjet)*sizeof(int));
    for(int k = 0; k<nbObjet; k++)
        result[k]=k;
    /** tri du tableau des valeurs et de celui des indices */
    for (i=0; i<nbObjet; i++){
        for (j=i+1; j<nbObjet; j++){
            if (tabValeur[i]<tabValeur[j]) { /* échange de valeurs */
                tempValeur=tabValeur[i];
                tempNumero = result[i];
                tabValeur[i]=tabValeur[j];
                result[i]=result[j];
                tabValeur[j]=tempValeur;
                result[j]=tempNumero;
            }
        }
    }
    free(tabValeur);
    return result;
}

/** retourne la liste de des valeurs de tous les objets d'une instance
*@param instance * row : l'instance dans laquelle on regarde les objets
*@return : double * listeValeur : la liste des valeurs de tous les objets de l'instance
*/
double * listeValeurs(instance * row){
    int nbObjet = row->nbObjet;
    double * listValeur = (double*)malloc((size_t)(nbObjet)*sizeof(double));
    for (int i=0; i<nbObjet; i++)
            listValeur[i]=(double)row->listeObj[i].valeur;
    return listValeur;
}

/** fonction ordonne les numéro d'objet selon l'odre décroissant de leurs valeurs.
* @param : instance * row : l'instance ou l'on va chercher les valeurs des objets.
* @return : int * result : la permutation des objet ordonnancer dans l'ordre décroissant de leurs valeurs.
*/
int * ordonnancement_valeur(instance *row){
    /** tableau qui contient les valeurs des objets */
    double * tabValeur = listeValeurs(row);
    return ordonnancement(row->nbObjet, tabValeur);
}

/** ordonner de façon aléatoire l'ensemble des indices des objets présents dans une instance
* @param instance * row : l'instance dans laquelle on trouvera le nombre d'objet.
* @return int * permutation : liste des indices des objets trié dans un ordre aléatoire.
*/
int * ordonnancement_alea(instance *row){
    int taille=row->nbObjet;
    int alea = 0;

    /** tableau des indices */
    int * tableau = (int*)malloc((size_t)(taille)*sizeof(int));
    for(int k = 0; k<row->nbObjet; k++)
        tableau[k]=k;

    int * permutation = (int*)malloc((size_t)(row->nbObjet)*sizeof(int));

    /** on tire au sort un indice d'objet, on le place dans permutation
    * on réduit la taille du tableau d'indice et on supprime l'indice sélectionné.
    * on répète l'opération avec les indices restant dans le tableau.
    */
    for(int i=0; i<row->nbObjet; i++){
        alea = rand()%taille;
        permutation[i]=tableau[alea];
        --taille;
        for(int j=alea; j<taille; j++)
            tableau[j]=tableau[j+1];
    }
    free(tableau);
    return permutation;
}

/** retourne la liste de ratio pour tous les objets d'une instance
* les ratio correspondent à la valeur de l'objet sur la somme de leurs poids
*@param instance * row : l'instance dans laquelle on regarde les objets
*@return : double * listeRatio : la liste des ratios de tous les objets de l'instance
*/
double * listeRatioVal(instance * row){
    int nbObjet = row->nbObjet;
    int nbDim = row->nbDimension;
    double * listRatio = (double*)malloc((size_t)(nbObjet)*sizeof(double));
    for (int i=0; i<nbObjet; i++){
        int somme = 0;
        for (int j=0; j<nbDim; j++)
            somme += row->listeObj[i].listePoids[j];
        listRatio[i]=(double)(row->listeObj[i].valeur)/somme;
    }

    return listRatio;
}

/** ordonne les indice des objet en fonction du ratio de leur valeur et de la somme des poids des dimensions
* @param instance * row : instance dans laquelle on cherche les objets
* @return int * permutation : la permutation des indices des objets
*/
int * ordonnancement_ratio_valeurObjet_sommePoids(instance * row){
    /** tableau qui contient les valeurs des objets */
    double * tabValeur = listeRatioVal(row);
    return ordonnancement(row->nbObjet, tabValeur);
}


/** retourne la liste de ratio pour tous les objets d'une instance
* les ratio correspondent à la valeur de l'objet sur le poid de l'objet dans la dimension critique
*@param instance * row : l'instance dans laquelle on regarde les objets
*@return : double * listeRatio : la liste des ratios de tous les objets de l'instance
*/
double * listeRatioValCrit(instance * row){
    int nbObjet = row->nbObjet;
    int nbDim = row->nbDimension;
    int sommePoids = 0;
    int poidCritique = 0;
    int numDimCrit = 0;
    int max = 0;


    double rationDim[nbDim];
    double * listRatio = (double*)malloc((size_t)(nbObjet)*sizeof(double));

    /** pour calculer la dim critique on regarde pour chaque dimension
    * la somme des poids des objets dans la dimension en question et on fait
    * le ratio entre la somme de ses poid et le poid max de la dimension.
    */
    for (int j=0; j<nbDim; j++){
        sommePoids = 0;
        for (int i=0; i<nbObjet; i++)
            sommePoids=sommePoids+(row->listeObj[i].listePoids[j]);
        rationDim[j]=(double)(sommePoids/(row->poidMax[j]));
    }

    /** on cherche le ratio le plus grand, ce ratio correspond a celui de la dimension critique */
    for (int j=0; j<nbDim; j++){
        if(rationDim[j]>max){
            max = rationDim[j];
            numDimCrit = j;
        }
    }

    /** pour chaque objet on calcul le ratio entre sa valeur et son poid dans la dimension critique */
    for (int i=0; i<nbObjet; i++){
        poidCritique=row->listeObj[i].listePoids[numDimCrit];
        listRatio[i]=(double)(row->listeObj[i].valeur)/poidCritique;
    }

    return listRatio;
}

/** ordonne les indice des objet en fonction du ratio de leur valeur et du poids de la dimension la plus critique
* @param instance * row : instance dans laquelle on cherche les objets
* @return int * permutation : la permutation des indices des objets
*/
int * ordonnancement_ratio_valeurObjet_dimensionCritique(instance * row){
    /** tableau qui contient les valeurs des objets */
    double * tabValeur = listeRatioValCrit(row);

    return ordonnancement(row->nbObjet, tabValeur);
}

/** fonction qui permet de recalculer une permutation en fonction d'une nouvelle dimension critique
*@param : instance * row : l'instance pour laquelle on cherche la nouvelle permutation
*@param int * liste : la permutation que l'on veut moidifier
*@param int * capMax : la liste des nouvelles capacité max permettant de calculer la nouvelle dimension critique
*@param int nbObjet : le nombre d'objet de la permutation
*/
int * updateDynamique(instance * row, int * liste, int * capMax, int nbObjet){
    int nbDim = row->nbDimension;
    double sommePoids = 0.0;
    int numDimCrit = 0;
    int poidCritique = 0;
    int max = 0;

    double rationDim[nbDim];
    double * listRatio = (double*)malloc((size_t)(nbObjet-1)*sizeof(double));

    retirerElementTab(liste, nbObjet);

    for (int j=0; j<nbDim; j++){
        sommePoids = 0.0;
        for (int i=0; i<nbObjet-1; i++)
            sommePoids=sommePoids+row->listeObj[liste[i]].listePoids[j];
        rationDim[j]=(double)(sommePoids/(capMax[j]));
    }

    for (int j=0; j<nbDim; j++){
        if(rationDim[j]>max){
            max = rationDim[j];
            numDimCrit = j;
        }
    }

    for (int i=0; i<nbObjet-1; i++){
        poidCritique=row->listeObj[liste[i]].listePoids[numDimCrit];
        listRatio[i]=(double)((row->listeObj[liste[i]].valeur)/poidCritique);
    }

    free(liste);

    return ordonnancement(nbObjet-1, listRatio);

}

/** ordonne une permutation en fonction de la taille totale des objets
* on essaye de mettre le plus d'objet possible dans le sac.
*/
int * ordonnancement_invention(instance * row){
    int listeDim[row->nbObjet];
    int * result = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    int sommeDim = 0;

    double tempValeur = 0.0;
    int tempNumero = 0;

    for(int i = 0; i<row->nbObjet; i++){
        sommeDim = 0;
        for(int j = 0; j<row->nbDimension; j++)
            sommeDim += row->listeObj[i].listePoids[j];
        listeDim[i] = sommeDim;
    }

    /** tableau qui contient le numéro des objets */
    for(int k = 0; k<row->nbObjet; k++)
        result[k]=k;
    /** tri du tableau des valeurs et de celui des indices */
    for (int i=0; i<row->nbObjet; i++){
        for (int j=i+1; j<row->nbObjet; j++){
            if (listeDim[i]>listeDim[j]) { /* échange de valeurs */
                tempValeur=listeDim[i];
                tempNumero = result[i];
                listeDim[i]=listeDim[j];
                result[i]=result[j];
                listeDim[j]=tempValeur;
                result[j]=tempNumero;
            }
        }
    }
    return result;

}

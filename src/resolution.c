#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include "../header/instance.h"
#include "../header/objet.h"
#include "../header/tool.h"
#include "../header/parseur.h"
#include "../header/decodeur.h"
#include "../header/heuristique.h"
#include "../header/resolution.h"
#include "../header/matheuristique.h"
#include "../header/ordonnancement.h"
#include "../header/genetique.h"

/** fonction qui va tester toutes les méthodes pour les deux codage pour chaque instance d'une instance DB
* écris dans un fichier de sortie : la meilleure fonction objective et le temps pour la calculer pour chaque instance.
* affiche dans la console : le n° de l'instance avec son nombre d'objet et son nombre de dimension et la meilleure solution trouver parmis toutes les méthodes
*/
void resolution(instanceDB * row){

    /** definition des solutions */
    solution * solDirecte = NULL;
    solution * solBestDirecte = NULL;
    solutionIndirecte * solIndirecte = NULL;
    solutionIndirecte * solBestIndirecte = NULL;

    /** definition fichier de sortie */
    FILE* sortie = NULL;
    sortie = fopen("sortie.txt","w");
    if(sortie==NULL){
        perror("erreur d'ouverture");
        exit(EXIT_FAILURE);
    }

    /** definition fichier d'un fichier de sortie excel pour le drive */
    FILE* tableur = NULL;
    tableur = fopen("tableur.csv","w");
    if(tableur==NULL){
        perror("erreur d'ouverture");
        exit(EXIT_FAILURE);
    }

    /** definition timer de début et de fin */
    struct timespec start, end;

    /** variables auxiliaires */
    /** definition du nombre de codage (indirecte + directe) et du nombre de méthode réalisée */
    int nbCodage = 2;
    int nbMethode = 10;
    /** définition des fonction objectives */
    int fSolDirecte = 0;
    int fBestSolDirecte = 0;
    int fSolIndirecte = 0;
    int fBestSolIndirecte = 0;
    /** defintion des timer pour les meilleure solutions */
    double bestSolDirecteTime = 0.0;
    double bestSolIndirecteTime = 0.0;
    /** numero de la meilleure méthode */
    int bestmethodeDirecte = 0;
    int bestmethodeIndirecte = 0;

    /** pour chaque instance on affiche son numero, son nb d'objet et son nb de dimension */
    for(int k = 0; k<row->nbInstance; k++){
        printf("\n\ninstance : %d, le nombre d'objet est : %d et le nombre de dimension est : %d\n", k, row->listeInstance[k]->nbObjet, row->listeInstance[k]->nbDimension);
        /** on initialise les solution et leurs fonction objective */
        fSolDirecte = 0;
        fSolIndirecte = 0;
        fBestSolDirecte = 0;
        fBestSolIndirecte = 0;
        solBestDirecte = solution_create(row->listeInstance[k]->nbObjet);
        solBestIndirecte = solutionIndirecte_create(row->listeInstance[k]->nbObjet);

        /** on calcul la meilleure soltuion pour chaque codage et chaque méthode
        * CHANGER LES CRITERES D'ARRET DES BOUCLES POUR TESTER DIFFERENTES METHODES ET CODAGE
        */
        for(int i = 0; i<nbCodage; i++){
            for(int j = 1; j<=nbMethode; j++){

                /** partie de code pour le codage directe */
                if(i==0){

                    /** début timer */
                    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
                    solDirecte = resolutionDirecte(row->listeInstance[k],j,20,20,30,0.5);
                    /**fin timer */
                    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

                    if(solDirecte!=NULL){
                        /** affichage solution */
                        printf("\n\nfct obj codage : %d méthode %d = %d\n\n",i,j,fonctionObjective_directe(solDirecte,row->listeInstance[k]));
                        afficher_directe(solDirecte, row->listeInstance[k]);
                        /** calcul et affiche le temps d'execution en seconde */
                        double time = (uint64_t)((end.tv_sec - start.tv_sec)*1000000+(end.tv_nsec - start.tv_nsec)/1000);
                        /** calcul de la fonction objective */
                        fSolDirecte = fonctionObjective_directe(solDirecte, row->listeInstance[k]);
                        /** si elle meilleure que celle de la méthode précédente, la fct obj best devient la fc obj courrante */
                        if(fSolDirecte >= fBestSolDirecte){
                            fBestSolDirecte = fSolDirecte;
                            solution_copy(solDirecte, solBestDirecte, row->listeInstance[k]->nbObjet);
                            bestSolDirecteTime = time;
                            bestmethodeDirecte = j;
                        }
                        free(solDirecte->tabBinaire);
                        free(solDirecte);
                    }
                }
                /** partie de code pour le codage indirecte */
                else if(i == 1){
                    /** début timer */
                    clock_gettime(CLOCK_MONOTONIC_RAW, &start);
                    solIndirecte = resolutionIndirecte(row->listeInstance[k],j,20,20,30,0.5);
                    /**fin timer */
                    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

                    if(solIndirecte!=NULL){
                        /**affichage de la solution */
                        printf("\n\nfct obj codage : %d méthode %d = %d\n\n",i,j,fonctionObjective_indirecte(solIndirecte,row->listeInstance[k]));
                        afficher_indirecte(solIndirecte);
                        /** calcul et affiche le temps d'execution en seconde */
                        double time = (uint64_t)((end.tv_sec - start.tv_sec)*1000000+(end.tv_nsec - start.tv_nsec)/1000);
                        /** calcul de la fonction objective */
                        fSolIndirecte = fonctionObjective_indirecte(solIndirecte, row->listeInstance[k]);
                        /** si elle meilleure que celle de la méthode précédente, la fct obj best devient la fc obj courrante */
                        if(fSolIndirecte >= fBestSolIndirecte){
                            fBestSolIndirecte = fSolIndirecte;
                            solution_indirecte_copy(solIndirecte, solBestIndirecte);
                            bestSolIndirecteTime = time;
                            bestmethodeIndirecte = j;
                        }
                        free(solIndirecte->solution);
                        free(solIndirecte);
                    }
                }
                else
                    exit(EXIT_FAILURE);

            }
        }

        /** on compare la meilleure solution directe avec la meilleure indirecte
        * on affiche la solution en conséquence dans la console et on écris dans les deux fichier de sortie
        */
        printf("\n\nMEILLEURE SOLUTION : ");
        if(fBestSolDirecte > fBestSolIndirecte){
            printf("%d\n", fBestSolDirecte);
            afficher_directe(solBestDirecte, row->listeInstance[k]);
            fprintf(sortie, " %d %d %lf : cod: 0 methode:%d\n",k, fBestSolDirecte, bestSolDirecteTime/1000000.0,bestmethodeDirecte);
            fprintf(tableur, "%d,%lf\n",fBestSolDirecte,bestSolDirecteTime/1000000.0);

        }
        else{
            printf("%d\n", fBestSolIndirecte);
            afficher_indirecte(solBestIndirecte);
            fprintf(sortie, " %d %d %lf : ",k, fBestSolIndirecte, bestSolIndirecteTime/1000000.0);
            if(fBestSolDirecte ==
                fBestSolIndirecte)
                fprintf(sortie, "codage 0 et 1 méthode : %d\n",bestmethodeIndirecte);
            else
                fprintf(sortie, "codage 1 méthode : %d\n",bestmethodeIndirecte);
            fprintf(tableur, "%d,%lf\n",fBestSolIndirecte,bestSolIndirecteTime/1000000.0);

        }

        free(solBestDirecte->tabBinaire);
        free(solBestDirecte);
        free(solBestIndirecte->solution);
        free(solBestIndirecte);
    }

    fclose(sortie);
    fclose(tableur);

}

/** fonction qui résout une instance pour un codage et une méthode donnée
* écris dans un fichier de sortie : la meilleure fonction objective et le temps pour la calculer pour chaque instance.
* affiche dans la console : le n° de l'instance avec son nombre d'objet et son nombre de dimension, et la solution
*@param : instanceDB * row : structure instanceDB contenant toutes les instances à résoudre
*@param : int numMathode : la méthode de résolution des instances
*@param int num codage : le codage souhaité pour résoudre
*/
void resolution_choix(instanceDB * row, int numMethode, int numCodage){

    /** definition des solutions */
    solution * solDirecte = NULL;
    solutionIndirecte * solIndirecte = NULL;

    /** definition fichier de sortie */
    FILE* sortie = NULL;
    sortie = fopen("sortie.txt","w");
    if(sortie==NULL){
        perror("erreur d'ouverture");
        exit(EXIT_FAILURE);
    }

    /** definition timer de début et de fin */
    struct timespec start, end;
    double time = 0.0;

    /** définition des fonction objectives */
    int fctObjective = 0;

    int nbIte = 0;
    int taillePopu = 0;
    int tailleTaboue = 0;
    double pMut = 0.0;

    if(numMethode == 8){
        printf("\nnombre d'itération\n");
        scanf("%d",&nbIte);
        printf("\ntaille liste Taboue\n");
        scanf("%d",&tailleTaboue);
    }

    if(numMethode == 9 || numMethode == 10){
        printf("\nnombre d'itération\n");
        scanf("%d",&nbIte);
        printf("\ntaille population\n");
        scanf("%d",&taillePopu);
        printf("\nprobabilité population\n");
        scanf("%lf",&pMut);
    }

    /** pour chaque instance on affiche son numero, son nb d'objet et son nb de dimension */
    for(int k = 0; k<row->nbInstance; k++){
        printf("\n\ninstance : %d, le nombre d'objet est : %d et le nombre de dimension est : %d\n", k, row->listeInstance[k]->nbObjet, row->listeInstance[k]->nbDimension);
        /** on initialise les solution et leurs fonction objective */
        fctObjective = 0;
        time = 0.0;


        /** partie de code pour le codage directe */
        if(numCodage==0){

            /** début timer */
            clock_gettime(CLOCK_MONOTONIC_RAW, &start);
            solDirecte = resolutionDirecte(row->listeInstance[k],numMethode,nbIte,tailleTaboue,taillePopu,pMut);
            /**fin timer */
            clock_gettime(CLOCK_MONOTONIC_RAW, &end);

            if(solDirecte!=NULL){
                /** affichage solution */
                fctObjective = fonctionObjective_directe(solDirecte,row->listeInstance[k]);
                printf("\n\nFONCTION OBJECTIVE = %d\n\n", fctObjective);
                afficher_directe(solDirecte, row->listeInstance[k]);
                /** calcul et affiche le temps d'execution en seconde */
                time = (uint64_t)((end.tv_sec - start.tv_sec)*1000000+(end.tv_nsec - start.tv_nsec)/1000);
                /** calcul de la fonction objective */
                solution_destroy(solDirecte);
            }
        }
        /** partie de code pour le codage indirecte */
        else if(numCodage == 1){
            /** début timer */
            clock_gettime(CLOCK_MONOTONIC_RAW, &start);
            solIndirecte = resolutionIndirecte(row->listeInstance[k],numMethode,nbIte,tailleTaboue,taillePopu,pMut);
            /**fin timer */
            clock_gettime(CLOCK_MONOTONIC_RAW, &end);

            if(solIndirecte!=NULL){
                /**affichage de la solution */
                fctObjective = fonctionObjective_indirecte(solIndirecte,row->listeInstance[k]);
                printf("\n\nFONCTION OBJECTIVE = %d\n\n", fctObjective);
                afficher_indirecte(solIndirecte);
                /** calcul et affiche le temps d'execution en seconde */
                time = (uint64_t)((end.tv_sec - start.tv_sec)*1000000+(end.tv_nsec - start.tv_nsec)/1000);
                solutionIndirecte_destroy(solIndirecte);
            }
        }
        else
            exit(EXIT_FAILURE);

        fprintf(sortie, "%d %d %lf\n",k, fctObjective, time/1000000.0);

    }

    fclose(sortie);

}

/** fonction qui créer une solution directe en fonction de la méthode donnée en paramètre
*@param : instance * row : l'instance pour laquelle on veut trouver une solution
*@param : int numMethode : le numéro de la méthode que l'on souhaite executer
*@return : solution * sol : la solution de l'instance .
*/
solution * resolutionDirecte(instance * row, int numMethode, int nbIte, int tailleTaboue, int tailePopu, double pMut){
    solution * sol = NULL;

    /** création de la solution */

    switch(numMethode){
        case 1 : sol = heuristiqueDirecte(row, 1); break;
        case 2 : sol = heuristiqueDirecte(row, 2); break;
        case 3 : sol = heuristiqueDirecte(row, 3); break;
        case 4 : sol = heuristiqueDirecte(row, 4); break;
        case 5 : sol = heuristiqueDirecteDynamique(row); break;
        case 6 : sol = heuristiqueDirecte(row, 5); break; //invention
        case 7 : sol = rechercheLocale_directe(row); break;
        case 8 : sol = rechercheTaboueDirecte(row, nbIte, tailleTaboue, 0); break;
        case 9 : sol = algoGenetique_directe(row,nbIte,tailePopu,pMut); break;
        case 10 : sol = algoGenetique_directe_rechercheLocale(row,nbIte,tailePopu,pMut); break;

    }

    return sol;

}

/** fonction qui créer une solution indirecte en fonction de la méthode donnée en paramètre
*@param : instance * row : l'instance pour laquelle on veut trouver une solution
*@param : int numMethode : le numéro de la méthode que l'on souhaite executer
*@return : solutionIndirecte * sol : la solution de l'instance .
*/
solutionIndirecte * resolutionIndirecte(instance * row, int numMethode, int nbIte, int tailleTaboue, int tailePopu, double pMut){
        solutionIndirecte * sol = NULL;
        int * permutation = NULL;
            /** création de la permutation et de la solution associée */
            switch(numMethode){
                case 1 :
                permutation = ordonnancement_valeur(row);
                sol = decoder(permutation, row);
                break;
                case 2 :
                permutation = ordonnancement_alea(row);
                sol = decoder(permutation, row);
                break;
                case 3 :
                permutation = ordonnancement_ratio_valeurObjet_sommePoids(row);
                sol = decoder(permutation, row);
                break;
                case 4 :
                permutation = ordonnancement_ratio_valeurObjet_dimensionCritique(row) ;
                sol = decoder(permutation, row);
                break;
                case 5 :
                permutation = ordonnancement_ratio_valeurObjet_dimensionCritique(row) ;
                sol = decoder(permutation, row);
                break;
                case 6 :
                permutation = ordonnancement_invention(row);
                sol = decoder(permutation,row);
                break;
                case 7 : sol = rechercheLocale_indirecte(row); break;
                case 8 : sol = rechercheTaboueIndirecte(row, nbIte, tailleTaboue, 0); break;
                case 9 : sol = algoGenetique_indirecte(row, nbIte, tailePopu, pMut); break;
                case 10 : sol = algoGenetique_indirecte_rechercheLocale(row, nbIte, tailePopu, pMut);

            }

            free(permutation);

        return sol;

}

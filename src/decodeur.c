#include "../header/decodeur.h"
#include "../header/solution.h"
#include "../header/instance.h"

/** cette fonction permet de trouver la solution pour une instance en fonction d'une permutation
* @param : int * permutation : la permutation qui va permettre de séterminer la solution
* @param : instance * row : l'instance pour laquelle on cherche une solution
* @return : solutionIndirecte * sol : la solution obtenue
*/
solutionIndirecte * decoder(int * permutation, instance * row){
    int numObj = 0;

    /** crétion et initialisation de la solution */
    solutionIndirecte * sol = (solutionIndirecte *)malloc(sizeof(solutionIndirecte));
    sol->solution = (int*)malloc((size_t)row->nbObjet*sizeof(int));
    sol->nbObjet = 0;

    /** init cap max par dimension */
    int capMax[row->nbDimension];
    for(int k=0; k<row->nbDimension; ++k)
        capMax[k]=row->poidMax[k];

    /** pour chaque objet on vérifie que toutes ses dimension son inférieur à la capacité restante de chaque dimension */
    for(int i = 0; i<row->nbObjet; i++){
        numObj = permutation[i];
        /** si boolean à 1, ça veut dire que tous les poids sont valide
        * on augmente le nb d'obj de la solution, et on ajout le num d'obj
        */
        if(ajoutSacPossible(row, numObj, capMax) == 1){
            sol->solution[sol->nbObjet] = numObj;
                /** on diminue les capacité restante pour chaque dimension dans le sac */
            for(int x=0; x<row->nbDimension; x++)
                capMax[x] = capMax[x]-((row->listeObj[numObj]).listePoids[x]);
            ++sol->nbObjet;
        }
    }

    return sol;
}

/** fonction permettant de tester si un objet peut entrer dans un sac
* on va comparer chacunes de ses dimensions avec les capacités restantes du sacs
* @param instance * row : l'instance dans laquelle on va cher les poids de l'objet que l'on veut tester
* @param int numObj : l'indice de l'objet pour lequel on veut connaitre les poids
* @param int * capMax : les capacité maximale du sac dans chacune des dimensions
* @return retourne 1 si l'ajout est possible, retourne 0 sinon
*/
int ajoutSacPossible(instance * row, int numObj, int * capMax){

    int j = 0;
    int poidObj = 0;


     while((j<(row->nbDimension))){
            /** on compare le poid de l'obj dans la dimension i et on le compare au poid restant dans le sac */
            poidObj = row->listeObj[numObj].listePoids[j];

            if(capMax[j]-poidObj < 0){
                return 0;
            }
            j++;
    }
    return 1;

}

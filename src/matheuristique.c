#include "../header/matheuristique.h"
#include "../header/instance.h"
#include "../header/solution.h"
#include "../header/heuristique.h"
#include "../header/decodeur.h"
#include "../header/tool.h"
#include "../header/ordonnancement.h"

/** cette fonction calcul une première solution et cheche la meilleure des solution dans le voisinnage de cette dernière
*on utilise pour cela un opérateur de voisinage : on ajoute un objet au fur et a mesure et on voit si la solution est possible
* @param : instance * row : l'instance pour laquelle on applique la recherche locale
* solution * BestSol : la meilleure solution directe trouver dans le voisinage
*/
solution * rechercheLocale_directe(instance * row){
    int nbObjet = row->nbObjet;
    /** definition des différentes solutions temporaires */
    solution * solCourante = heuristiqueDirecte(row,3);
    solution * BestSol = solution_create(nbObjet);
    solution_copy(solCourante, BestSol, nbObjet);
    solution * solVoisine = solution_create(nbObjet);
    solution * solBestVoisin = solution_create(nbObjet);
    /** definition des fonction objective associées à chaque solution */
    int fbest = fonctionObjective_directe(solCourante, row);
    int fprec = fonctionObjective_directe(solCourante, row);
    int fbestVoisin = 0;
    int fcourant = 0;
    /** boolean permettant de savoir quand il n'y a plus de meilleure solution dans le voisinage */
    int continu = 1;
    int temp = 0;
    /** tant que l'on trouve une meilleure solution, on créer une solution voisine */
    while(continu == 1){
        fbestVoisin = 0;
        /** operateur de voisinage : pour cahque objet, si on trouve un 0 on le transforme en 1 et on test sa validité */
        for(int i = 0; i<nbObjet; i++){
                /** on copie la solution courante dans la solution voisine que l'on veut créer */
                solution_copy(solCourante, solVoisine, nbObjet);
                /** dès qu'il y a un zero on le transforme en 1 : revient à ajouter un objet */
                if(solVoisine->tabBinaire[i]==0){
                    solVoisine->tabBinaire[i] = 1;
                    /** on test si la solution est possible et si ça fonction objetive est la meilleure qu'on est trouvé dans le voisinnage */
                    if((fonctionObjective_directe(solVoisine,row)>fbestVoisin) && (test_solution_directe(solVoisine, row) == 1)){
                            /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                            solution_copy(solVoisine, solBestVoisin, nbObjet);
                            fbestVoisin = fonctionObjective_directe(solVoisine, row);
                    }
                }

        }
        /** operateur de voisinage : permutation */
        for(int i = 0; i<nbObjet; i++){
            for(int j = i+1; j<nbObjet; j++){
                /** on copie la solution courante dans la solution voisine que l'on veut créer */
                solution_copy(solCourante, solVoisine, nbObjet);
                if(solVoisine->tabBinaire[i] != solVoisine->tabBinaire[j]){
                    temp =  solVoisine->tabBinaire[i];
                    solVoisine->tabBinaire[i] = solVoisine->tabBinaire[j];
                    solVoisine->tabBinaire[j] = temp;
                }

                /** on test si la solution est possible et si ça fonction objetive est la meilleure qu'on est trouvé dans le voisinnage */
                if((fonctionObjective_directe(solVoisine,row)>fbestVoisin) && (test_solution_directe(solVoisine, row) == 1)){
                        /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                        solution_copy(solVoisine, solBestVoisin, nbObjet);
                        fbestVoisin = fonctionObjective_directe(solVoisine, row);
                }
            }

        }
        /** la solution courante devient le meilleure voisin */
        fcourant = fbestVoisin;
        solution_copy(solBestVoisin, solCourante, nbObjet);

        /** si cette solution courante est la meilleure de toutes on la copie dans la meilleure des solutions.*/
        if(fcourant > fbest){
            fbest = fcourant;
            solution_copy(solCourante, BestSol, nbObjet);
        }
        /** si la fct obj de la solution courante et donc de la meilleure voisine est inférieur à la précédente on arrete la boucle */
        else if(fcourant <= fprec){
            continu = 0;
        }
        fprec = fcourant;
    }

    /** on libère l'espace mémoire occupée par les solutions temporaires */
    free(solVoisine->tabBinaire);
    free(solVoisine);
    free(solBestVoisin->tabBinaire);
    free(solBestVoisin);
    free(solCourante->tabBinaire);
    free(solCourante);

    return BestSol;

}

/** cette fonction calcul une première soltuion indirecte et cheche la meilleure des solution dans le voisinnage de cette dernière
*on utilise pour cela un opérateur de voisinage : on fait une permutation de deux objet dans la permutation de base.
* @param : instance * row : l'instance pour laquelle on applique la recherche locale
* solutionIndirecte * BestSol : la meilleure solution directe trouver dans le voisinage
*/
solutionIndirecte * rechercheLocale_indirecte(instance * row){

    /** on définie une permutation par ratio valeur objet et somme des poids, on calcul la solution indirecte associée */
    int * permutationCourante = ordonnancement_ratio_valeurObjet_sommePoids(row) ;
    solutionIndirecte * solCourante = decoder(permutationCourante, row);
    int nbObjet = solCourante->nbObjet;

    /** on définie les solution temporaires et la meilleure des soltions et de même pour leurs permutation correspondantes */
    solutionIndirecte * BestSol = solutionIndirecte_create(0);
    solution_indirecte_copy(solCourante, BestSol);
    int * permutationVoisine = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permutationVoisine, row->nbObjet);
    solutionIndirecte * solVoisine = NULL;
    solutionIndirecte * solBestVoisin = solutionIndirecte_create(0);
    int * permutationBestVoisin = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permutationBestVoisin, row->nbObjet);
    permutation_init(permutationBestVoisin, row->nbObjet);
    /** on définie les fonction objective associées à ces solutions */
    int fbest = fonctionObjective_indirecte(solCourante, row);
    int fprec = fonctionObjective_indirecte(solCourante, row);
    free(solCourante->solution);
    free(solCourante);
    int fbestVoisin = 0;
    int fcourant = 0;
    int continu = 1;
    int temp = 0;

    /** tant que l'on trouve une meilleure solution, on créer une solution voisine */
    while(continu == 1){
        fbestVoisin = 0;
        /** on échange chaque objet dans la permutation avec tous les autres objets un à un */
        for(int i = 0; i<nbObjet; i++){
            for(int j = 0; j<nbObjet; j++){
                permutation_copy(permutationCourante,permutationVoisine, row->nbObjet);
                temp = permutationVoisine[i];
                permutationVoisine[i]=permutationVoisine[j];
                permutationVoisine[j]=temp;
                /** création de la solution indirecte après la pemrutation de deux objets */
                solVoisine = decoder(permutationVoisine, row);
                /** on test si la solution est possible et si elle est meilleure que la meilleure du voisinnage */
                if(fonctionObjective_indirecte(solVoisine,row)>fbestVoisin){
                    /** si oui elle devient la meilleure solution voisine */
                    solution_indirecte_copy(solVoisine, solBestVoisin);
                    permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                    fbestVoisin = fonctionObjective_indirecte(solVoisine, row);
                }
                else if(fbestVoisin == 0){
                    /** si oui elle devient la meilleure solution voisine */
                    solution_indirecte_copy(solVoisine, solBestVoisin);
                    permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                    fbestVoisin = fonctionObjective_indirecte(solVoisine, row);
                }
                free(solVoisine->solution);
                free(solVoisine);
            }
        }

        /** la solution courante devient la meilleure des solutions voisines */
        fcourant = fbestVoisin;
        permutation_copy(permutationBestVoisin,permutationCourante,row->nbObjet);

        /** si elle c'est la meilleure de toute elle devient meilleure solution */
        if(fcourant > fbest){
            fbest = fcourant;
            solution_indirecte_copy(solBestVoisin, BestSol);
        }
        /** si elle n'est pas meilleure que la précédente on arrête la boucle */
        else if(fcourant <= fprec){
            continu = 0;
        }
        fprec = fcourant;
    }

    /** on libère l'espace mémoire occupée par les solutions temporaires */
    free(solBestVoisin->solution);
    free(solBestVoisin);
    free(permutationCourante);
    free(permutationVoisine);
    free(permutationBestVoisin);
    return BestSol;

}

/** algorithme de recherche taboue codage directe: calcul d'une solution de départ, pour un certain nb d'itération on calcul un voisinnage de solutions
* on regarde si la meilleure solution du voisinage est meilleure que la bestSolution, finalement on retourne la meilleure des solutions
*@param instance * row : instance pour laquelle on cheche la meilleur solution
*@param int nbIteMax : le nombre de fois que l'on ve calculer un voisinage
*@param int taille_listeTaboue : la taille de la liste taboue, sert à limiter l'opérateur de voisinage pour ne pas tomber sur les meme solutions
*@param int aspi : critère d'aspiration permettant de passer outre la liste taboue
*@return solution * BestSol : la meilleure solution trouvée
*/
solution * rechercheTaboueDirecte(instance * row, int nbIteMax, int taille_listeTaboue, int aspi){

    int nbObjet = row->nbObjet;
    /** definition des différentes solutions temporaires */
    solution * solCourante = rechercheLocale_directe(row);
    solution * BestSol = solution_create(nbObjet);
    solution_copy(solCourante, BestSol, nbObjet);
    solution * solVoisine = solution_create(nbObjet);
    solution * solBestVoisin = solution_create(nbObjet);
    /** definition des fonction objective associées à chaque solution */
    int fbest = fonctionObjective_directe(solCourante, row);
    int fbestVoisin = 0;
    int fcourant = 0;

    int listeTaboue[taille_listeTaboue];
    for(int i = 0; i<taille_listeTaboue; i++)
        listeTaboue[i]=taille_listeTaboue;
    int mvtUtil = 0;
    int compteurTaboue = 0;
    int k = 0;
    int temp = 0;

    /** tant que l'on trouve une meilleure solution, on créer une solution voisine */
    while(k < nbIteMax){
        fbestVoisin = 0;
        /** operateur de voisinage : pour cahque objet, si on trouve un 0 on le transforme en 1 et on test sa validité */
        for(int i = 0; i<nbObjet; i++){
            if(trouverElement(listeTaboue, i, 0, taille_listeTaboue)==0 || aspi==1){
                /** on copie la solution courante dans la solution voisine que l'on veut créer */
                solution_copy(solCourante, solVoisine, nbObjet);
                /** dès qu'il y a un zero on le transforme en 1 : revient à ajouter un objet */
            if(solVoisine->tabBinaire[i]==0){
                solVoisine->tabBinaire[i] = 1;
                if(trouverElement(listeTaboue, i, 0, taille_listeTaboue)==0){
                    /** on test si la solution est possible et si ça fonction objetive est la meilleure qu'on est trouvé dans le voisinnage */
                    if((fonctionObjective_directe(solVoisine,row)>fbestVoisin) && (test_solution_directe(solVoisine, row) == 1)){
                            /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                            solution_copy(solVoisine, solBestVoisin, nbObjet);
                            fbestVoisin = fonctionObjective_directe(solVoisine, row);
                            mvtUtil = i;
                    }
                    else if(test_solution_directe(solVoisine, row) == 1 && fbestVoisin == 0){
                            /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                            solution_copy(solVoisine, solBestVoisin, nbObjet);
                            fbestVoisin = fonctionObjective_directe(solVoisine, row);
                            mvtUtil = i;
                    }
                }
                else if(fonctionObjective_directe(solVoisine,row)>fbest && test_solution_directe(solVoisine,row)==1){
                        solution_copy(solVoisine, solBestVoisin, nbObjet);
                        fbestVoisin = fonctionObjective_directe(solVoisine,row);
                        mvtUtil = i;
                }
            }
            }
        }
        /** operateur de voisinage : permutation */
        for(int i = 0; i<nbObjet; i++){
            for(int j = i+1; j<nbObjet; j++){
                if(trouverElement(listeTaboue, i, 0, taille_listeTaboue)==0 || aspi==1){
                    /** on copie la solution courante dans la solution voisine que l'on veut créer */
                    solution_copy(solCourante, solVoisine, nbObjet);
                    if(solVoisine->tabBinaire[i] != solVoisine->tabBinaire[j]){
                            temp =  solVoisine->tabBinaire[i];
                            solVoisine->tabBinaire[i] = solVoisine->tabBinaire[j];
                            solVoisine->tabBinaire[j] = temp;


                        if(trouverElement(listeTaboue, i, 0,taille_listeTaboue)==0){
                            /** on test si la solution est possible et si ça fonction objetive est la meilleure qu'on est trouvé dans le voisinnage */
                            if((fonctionObjective_directe(solVoisine,row)>fbestVoisin) && (test_solution_directe(solVoisine, row) == 1)){
                                    /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                                    solution_copy(solVoisine, solBestVoisin, nbObjet);
                                    fbestVoisin = fonctionObjective_directe(solVoisine, row);
                                    mvtUtil = i;
                            }
                            else if(test_solution_directe(solVoisine, row) == 1 && fbestVoisin == 0){
                                    /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                                    solution_copy(solVoisine, solBestVoisin, nbObjet);
                                    fbestVoisin = fonctionObjective_directe(solVoisine, row);
                                    mvtUtil = i;
                            }
                        }
                        else if(fonctionObjective_directe(solVoisine,row)>fbest && test_solution_directe(solVoisine,row)==1){
                                solution_copy(solVoisine, solBestVoisin, nbObjet);
                                fbestVoisin = fonctionObjective_directe(solVoisine,row);
                                mvtUtil = i;
                        }
                    }
                }
            }

        }

        /** la solution courante devient le meilleure voisin */
        fcourant = fbestVoisin;
        solution_copy(solBestVoisin, solCourante, nbObjet);

        listeTaboue[compteurTaboue] = mvtUtil;
        if(compteurTaboue==taille_listeTaboue)
            compteurTaboue=0;
        else
            compteurTaboue++;


        /** si cette solution courante est la meilleure de toutes on la copie dans la meilleure des solutions.*/
        if(fcourant > fbest){
            fbest = fcourant;
            solution_copy(solCourante, BestSol, nbObjet);
            k = 0;
        }

        k++;
    }

    /** on libère l'espace mémoire occupée par les solutions temporaires */
    free(solVoisine->tabBinaire);
    free(solVoisine);
    free(solBestVoisin->tabBinaire);
    free(solBestVoisin);
    free(solCourante->tabBinaire);
    free(solCourante);

    return BestSol;

}

/** algorithme de recherche taboue codage indirecte : calcul d'une solution de départ, pour un certain nb d'itération on calcul un voisinnage de solutions
* on regarde si la meilleure solution du voisinage est meilleure que la bestSolution, finalement on retourne la meilleure des solutions
*@param instance * row : instance pour laquelle on cheche la meilleur solution
*@param int nbIteMax : le nombre de fois que l'on ve calculer un voisinage
*@param int taille_listeTaboue : la taille de la liste taboue, sert à limiter l'opérateur de voisinage pour ne pas tomber sur les meme solutions
*@param int aspi : critère d'aspiration permettant de passer outre la liste taboue
*@return solutionIndirecte * BestSol : la meilleure solution trouvée
*/
solutionIndirecte * rechercheTaboueIndirecte(instance * row, int nbIteMax, int taille_listeTaboue, int aspi){
    /** on définie une permutation par ratio valeur objet et somme des poids, on calcul la solution indirecte associée */
    int * permutationCourante = ordonnancement_ratio_valeurObjet_sommePoids(row);
    solutionIndirecte * solCourante = decoder(permutationCourante, row);
    /** definition des différentes solutions temporaires et leurs permutations respectives */
    solutionIndirecte * BestSol = solutionIndirecte_create(0);
    solution_indirecte_copy(solCourante, BestSol);
    int * permutationVoisine = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permutationVoisine, row->nbObjet);
    solutionIndirecte * solVoisine = NULL;
    solutionIndirecte * solBestVoisin = solutionIndirecte_create(0);
    int * permutationBestVoisin = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permutationBestVoisin, row->nbObjet);

    /** definition des fonction objective associées à chaque solution */
    int fbest = fonctionObjective_indirecte(solCourante, row);
    free(solCourante->solution);
    free(solCourante);
    int fbestVoisin = 0;
    int fcourant = 0;

    /** définition de la liste taboue */
    int listeTaboue[taille_listeTaboue];
    for(int i = 0; i<taille_listeTaboue; i++)
        listeTaboue[i]=taille_listeTaboue;
    int mvtUtil[2];
    mvtUtil[0]=0;
    mvtUtil[1]=0;
    int compteurTaboue = 0;
    /**variables auxiliaires */
    int temp = 0;
    int k = 0;

    /** tant que l'on trouve une meilleure solution, on créer une solution voisine */
    while(k < nbIteMax){
        fbestVoisin = 0;
        /** operateur de voisinage : on échange chaque élement d'une permutaion et on créer une solution à partir de celle ci*/
        for(int i = 0; i<row->nbObjet; i++){
            for(int j = i+1; j<row->nbObjet; j++){

                if(trouverTabouIndirect(listeTaboue, mvtUtil, taille_listeTaboue)==0 || aspi==1){
                    /** échange de deux objet dans la permutation */
                    permutation_copy(permutationCourante,permutationVoisine, row->nbObjet);
                    temp = permutationVoisine[i];
                    permutationVoisine[i]=permutationVoisine[j];
                    permutationVoisine[j]=temp;

                    /** création de la solution indirecte après la pemrutation de deux objets */
                    solVoisine = decoder(permutationVoisine, row);

                    if(trouverTabouIndirect(listeTaboue, mvtUtil, taille_listeTaboue)==0){

                        /** on test si la solution est possible et si elle est meilleure que la meilleure du voisinnage */
                        if(fonctionObjective_indirecte(solVoisine,row)>fbestVoisin){
                            /** si oui elle devient la meilleure solution voisine */
                            solution_indirecte_copy(solVoisine, solBestVoisin);
                            permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                            fbestVoisin = fonctionObjective_indirecte(solVoisine, row);
                            mvtUtil[0] = i;
                            mvtUtil[1]= j;
                        }
                        else if(fbestVoisin == 0){
                            /** si oui elle devient la meilleure solution voisine */
                            solution_indirecte_copy(solVoisine, solBestVoisin);
                            permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                            fbestVoisin = fonctionObjective_indirecte(solVoisine, row);
                            mvtUtil[0] = i;
                            mvtUtil[1]= j;
                        }
                    }
                    else if(fonctionObjective_indirecte(solVoisine,row)>fbest){
                        solution_indirecte_copy(solVoisine, solBestVoisin);
                        permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                        fbestVoisin = fonctionObjective_indirecte(solVoisine,row);
                        mvtUtil[0] = i;
                        mvtUtil[1]= j;
                    }
                    free(solVoisine->solution);
                    free(solVoisine);
                }
            }

        }
        /** la solution courante devient le meilleure voisin */
        permutation_copy(permutationBestVoisin,permutationCourante,row->nbObjet);

        if(compteurTaboue==taille_listeTaboue)
            compteurTaboue = 0;
        listeTaboue[compteurTaboue] = mvtUtil[0];
        compteurTaboue++;
        listeTaboue[compteurTaboue] = mvtUtil[1];
        compteurTaboue++;

        /** si cette solution courante est la meilleure de toutes on la copie dans la meilleure des solutions.*/
        if(fbestVoisin > fbest){
            fbest = fcourant;
            solution_indirecte_copy(solBestVoisin, BestSol);
            k = 0;
        }

        k++;
    }

    /** on libère l'espace mémoire occupée par les solutions temporaires */
    free(solBestVoisin->solution);
    free(solBestVoisin);
    free(permutationCourante);
    free(permutationVoisine);
    free(permutationBestVoisin);

    return BestSol;
}



/** cette fonction cherche la meilleure des solution dans le voisinage d'une solution directe donnée
*on utilise pour cela un opérateur de voisinage : on ajoute un objet au fur et a mesure et on voit si la solution est possible
* @param : instance * row : l'instance pour laquelle on applique la recherche locale
* solution * BestSol : la meilleure solution directe trouver dans le voisinage
*/
void rechercheLocale_directe_Invention(solution * sol, instance * row){
    int nbObjet = row->nbObjet;
    /** definition des différentes solutions temporaires */
    solution * solCourante = solution_create(row->nbObjet);
    solution_copy(sol,solCourante,row->nbObjet);
    solution * BestSol = solution_create(nbObjet);
    solution_copy(solCourante, BestSol, nbObjet);
    solution * solVoisine = solution_create(nbObjet);
    solution * solBestVoisin = solution_create(nbObjet);
    /** definition des fonction objective associées à chaque solution */
    int fbest = fonctionObjective_directe(solCourante, row);
    int fprec = fonctionObjective_directe(solCourante, row);
    int fbestVoisin = 0;
    int fcourant = 0;
    /** boolean permettant de savoir quand il n'y a plus de meilleure solution dans le voisinage */
    int continu = 1;
    int temp = 0;
    /** tant que l'on trouve une meilleure solution, on créer une solution voisine */
    while(continu == 1){
        fbestVoisin = 0;
        /** operateur de voisinage : pour cahque objet, si on trouve un 0 on le transforme en 1 et on test sa validité */
        for(int i = 0; i<nbObjet; i++){
                /** on copie la solution courante dans la solution voisine que l'on veut créer */
                solution_copy(solCourante, solVoisine, nbObjet);
                /** dès qu'il y a un zero on le transforme en 1 : revient à ajouter un objet */
                if(solVoisine->tabBinaire[i]==0){
                    solVoisine->tabBinaire[i] = 1;
                    /** on test si la solution est possible et si ça fonction objetive est la meilleure qu'on est trouvé dans le voisinnage */
                    if((fonctionObjective_directe(solVoisine,row)>fbestVoisin) && (test_solution_directe(solVoisine, row) == 1)){
                            /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                            solution_copy(solVoisine, solBestVoisin, nbObjet);
                            fbestVoisin = fonctionObjective_directe(solVoisine, row);
                    }
                }

        }
        /** operateur de voisinage : permutation */
        for(int i = 0; i<nbObjet; i++){
            for(int j = i+1; j<nbObjet; j++){
                /** on copie la solution courante dans la solution voisine que l'on veut créer */
                solution_copy(solCourante, solVoisine, nbObjet);
                if(solVoisine->tabBinaire[i] != solVoisine->tabBinaire[j]){
                    temp =  solVoisine->tabBinaire[i];
                    solVoisine->tabBinaire[i] = solVoisine->tabBinaire[j];
                    solVoisine->tabBinaire[j] = temp;
                }

                /** on test si la solution est possible et si ça fonction objetive est la meilleure qu'on est trouvé dans le voisinnage */
                if((fonctionObjective_directe(solVoisine,row)>fbestVoisin) && (test_solution_directe(solVoisine, row) == 1)){
                        /** si c'est les cas on copy la solution voisine créée dans la meilleure solution voisine*/
                        solution_copy(solVoisine, solBestVoisin, nbObjet);
                        fbestVoisin = fonctionObjective_directe(solVoisine, row);
                }
            }

        }
        /** la solution courante devient le meilleure voisin */
        fcourant = fbestVoisin;
        solution_copy(solBestVoisin, solCourante, nbObjet);

        /** si cette solution courante est la meilleure de toutes on la copie dans la meilleure des solutions.*/
        if(fcourant > fbest){
            fbest = fcourant;
            solution_copy(solCourante, BestSol, nbObjet);
        }
        /** si la fct obj de la solution courante et donc de la meilleure voisine est inférieur à la précédente on arrete la boucle */
        else if(fcourant <= fprec){
            continu = 0;
        }
        fprec = fcourant;
    }

    /** on libère l'espace mémoire occupée par les solutions temporaires */
    free(solVoisine->tabBinaire);
    free(solVoisine);
    free(solBestVoisin->tabBinaire);
    free(solBestVoisin);
    free(solCourante->tabBinaire);
    free(solCourante);

    solution_copy(BestSol, sol, row->nbObjet);
    solution_destroy(BestSol);
}

/** cette fonction cherche la meilleure des solution dans le voisinage d'une solution indirecte donnée
*on utilise pour cela un opérateur de voisinage : on fait une permutation de deux objet dans la permutation de base.
* @param : instance * row : l'instance pour laquelle on applique la recherche locale
* solutionIndirecte * BestSol : la meilleure solution directe trouver dans le voisinage
*/
void rechercheLocale_indirecte_invention(int * permu, solutionIndirecte* sol, instance * row){

    /** on définie une permutation par ratio valeur objet et somme des poids, on calcul la solution indirecte associée */
    int * permutationCourante = permutation_create(row);
    permutation_copy(permu, permutationCourante, row->nbObjet);
    solutionIndirecte * solCourante = solutionIndirecte_create(0);
    solution_indirecte_copy(sol,solCourante);
    int nbObjet = solCourante->nbObjet;

    /** on définie les solution temporaires et la meilleure des soltions et de même pour leurs permutation correspondantes */
    solutionIndirecte * BestSol = solutionIndirecte_create(0);
    solution_indirecte_copy(solCourante, BestSol);
    int * permutationVoisine = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permutationVoisine, row->nbObjet);
    solutionIndirecte * solVoisine = NULL;
    solutionIndirecte * solBestVoisin = solutionIndirecte_create(0);
    int * permutationBestVoisin = (int *)malloc((size_t)(row->nbObjet)*sizeof(int));
    permutation_init(permutationBestVoisin, row->nbObjet);
    permutation_init(permutationBestVoisin, row->nbObjet);
    /** on définie les fonction objective associées à ces solutions */
    int fbest = fonctionObjective_indirecte(solCourante, row);
    int fprec = fonctionObjective_indirecte(solCourante, row);
    free(solCourante->solution);
    free(solCourante);
    int fbestVoisin = 0;
    int fcourant = 0;
    int continu = 1;
    int temp = 0;

    /** tant que l'on trouve une meilleure solution, on créer une solution voisine */
    while(continu == 1){
        fbestVoisin = 0;
        /** on échange chaque objet dans la permutation avec tous les autres objets un à un */
        for(int i = 0; i<nbObjet; i++){
            for(int j = 0; j<nbObjet; j++){
                permutation_copy(permutationCourante,permutationVoisine, row->nbObjet);
                temp = permutationVoisine[i];
                permutationVoisine[i]=permutationVoisine[j];
                permutationVoisine[j]=temp;
                /** création de la solution indirecte après la pemrutation de deux objets */
                solVoisine = decoder(permutationVoisine, row);
                /** on test si la solution est possible et si elle est meilleure que la meilleure du voisinnage */
                if(fonctionObjective_indirecte(solVoisine,row)>fbestVoisin){
                    /** si oui elle devient la meilleure solution voisine */
                    solution_indirecte_copy(solVoisine, solBestVoisin);
                    permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                    fbestVoisin = fonctionObjective_indirecte(solVoisine, row);
                }
                else if(fbestVoisin == 0){
                    /** si oui elle devient la meilleure solution voisine */
                    solution_indirecte_copy(solVoisine, solBestVoisin);
                    permutation_copy(permutationVoisine, permutationBestVoisin, row->nbObjet);
                    fbestVoisin = fonctionObjective_indirecte(solVoisine, row);
                }
                free(solVoisine->solution);
                free(solVoisine);
            }
        }

        /** la solution courante devient la meilleure des solutions voisines */
        fcourant = fbestVoisin;
        permutation_copy(permutationBestVoisin,permutationCourante,row->nbObjet);

        /** si elle c'est la meilleure de toute elle devient meilleure solution */
        if(fcourant > fbest){
            fbest = fcourant;
            solution_indirecte_copy(solBestVoisin, BestSol);
        }
        /** si elle n'est pas meilleure que la précédente on arrête la boucle */
        else if(fcourant <= fprec){
            continu = 0;
        }
        fprec = fcourant;
    }

    /** on libère l'espace mémoire occupée par les solutions temporaires */
    free(solBestVoisin->solution);
    free(solBestVoisin);
    free(permutationCourante);
    free(permutationVoisine);
    free(permutationBestVoisin);
    solution_indirecte_copy(BestSol,sol);
    solutionIndirecte_destroy(BestSol);

}

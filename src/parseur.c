#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include "../header/instance.h"
#include "../header/listObjet.h"
#include "../header/objet.h"
#include "../header/tool.h"
#include "../header/parseur.h"
#include "../header/solution.h"
#include "../header/decodeur.h"

/** fonction qui prend en entrée un fichier d'instances formaté et qui créer une structure instanceDB associée
*@param : char* filename : le nom du fichier que l'on veut parser
*@return : instanceDB * row : la structure instanceDB contenant toute les instances du fichier
*/
instanceDB * parser(char* filename)
{
	int nbInstance = 0;

    /** definition des fichier d'entrée et sortie */
    FILE* fichier = NULL;

    /**ouverture du fichier d'entrée */
    fichier = fopen(filename,"r");
    if(fichier==NULL){
        perror("erreur d'ouverture");
        exit(EXIT_FAILURE);
    }

    /** lecture du nombre d'instance */
    fscanf(fichier, "%d", &nbInstance);
    printf("\nle nombre d'instance est : %d\n",nbInstance);
    instanceDB * row = (instanceDB*)malloc(sizeof(instanceDB));
    row->nbInstance = nbInstance;
    row->listeInstance = (instance **)malloc((size_t)(row->nbInstance)*sizeof(instance*));

    /** pour chaque instance on lit l'instance */
    for(int i=0; i<nbInstance; i++){
        /** instancie une instance */
        row->listeInstance[i] = readInstance(fichier);
    }

    fclose(fichier);

    return row;
}

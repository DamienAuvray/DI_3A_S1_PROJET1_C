/***********************************************************
* différentes fonctions permettant de manipuler des tableaux
************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include "../header/tool.h"
#include "../header/heuristique.h"

/** créer un tableau d'entier à pqrtir d'un fichier en fonction du nombre d'entier que l'on souhaite lire
*@param FILE * fichier : le fichier dans lequel on veut lire le tableau
*@param int count : le nombre d'entier que l'on veut lire dans le fichier
*@return int * tab : le tableau lu dans le fichier alloué sur le tas
*/
int * read_tab(FILE* fichier, int count){
    int * tab = (int*) malloc((size_t)count*sizeof(int));
    for(int i=0; i<count; i++){
        fscanf(fichier, "%d", (tab+i));
    }

    return tab;
}

/** initialise un tableau d'entier
*@param int * tab : le tableau à initialiser
*@param int taille : le nombre d'élément du tableau
*/
void init_tab(int * tab, int taille){
    for(int i = 0; i<taille; i++)
        tab[i]=0;
}

/** retire le premier élément d'un tableau (ne modifie pas sa taille, c'est à l'utilisateur de faire attention)
*@param int * tab : le tableau dans lequel on veut retirer le premier élément
*@param int taille : la taille du tableau
*/
void retirerElementTab(int * tab, int taille){
    for(int i = 0; i<taille-1; i++)
        tab[i]=tab[i+1];
}

/** détermine si un muvement est taboue (utilisé pour recherche taboue indirecte)
* @param int * liste : la liste taboue
* @param int * mvt : un tableau de deux case correspondant aux deux élément qui ont permuté (operateur de voisinage)
* @param int taille : la taille de la liste taboue
* @return : retourne un si le mvt est taboue et 0 si il ne l'est pas
*/
int trouverTabouIndirect(int * liste, int * mvt, int taille){
    for(int i = 0; i<taille; i++){
        if(liste[i]==mvt[0]){
            if(i==taille-1){
                if(liste[0]==mvt[1])
                return 1;
            }
            else{
                if(liste[i+1]==mvt[1])
                return 1;
            }
        }
    }
    return 0;
}

/** boolean déterminant si un entier est présent dans une partie d'une liste d'entier
*@param int * liste : la liste dans laquelle on cherche l'élément
*@param int element : l'entier que l'on cherche dans le morceau de liste
*@param int debut : l'indice de départ dans la liste ou l'on cherche l'élément
*@param int fin : l'indice de fin dans la liste ou l'on cherche l'élément
*@return : retourne 1 si l'élément est présent, 0 sinon
*/
int trouverElement(int * liste, int element, int debut, int fin){
    for(int i = debut; i<fin; i++){
        if(liste[i]==element)
            return 1;
    }
    return 0;
}

/** trouve l'indice d'un morceau d'une liste d'entier où l'on a trouvé un entier demandé
*@param int * liste : la liste où l'on cherche l'entier
*@param int element : l'entier dont on cherche la position dans le morceau de liste
*@param int debut : l'indice de début de recherche dans la liste
*@param int fin : l'indice de fin de recherche dans la liste
*/
int trouverIndice(int * liste, int element, int debut, int fin){
    int indice = 0;
    for(int i = debut; i<fin; i++){
        if(liste[i]==element)
            return i;
    }
    return indice;
}


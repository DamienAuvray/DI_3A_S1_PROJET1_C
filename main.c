#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include "./header/instance.h"
#include "./header/listObjet.h"
#include "./header/objet.h"
#include "./header/tool.h"
#include "./header/parseur.h"
#include "./header/decodeur.h"
#include "./header/heuristique.h"
#include "./header/resolution.h"

/** instancie une structure instanceDB contenant toute les instances d'un fichier
* la resolution permet d'executer l'ensembles des methode dans les 2 codage, de retourner la meilleure methode, et ecrire dans un fichier de sortie
* une fois l'ensemble des instances traitées, on détruit l'instanceDB
*/
int main(int argc, char * argv[])
{
    srand(time(NULL));
    int choix = 3;
    char* filename="MKP-Instances/_mknapcb1_res.txt";
    int numCodage = 0;
    int numMethode = 0;

    /** on parse le fichier et on le place dans une instanceDB */
    instanceDB * row = parser(filename);

    do{
        printf("\n\nresoudre toute les méthodes : 0, choix de la méthode : 1, quitter : 2\n");
        scanf("%d", &choix);

        if(choix == 0){
            /** on trouve la meileure solution pour chaque codages et méthodes pour chaque instance et on écris dans un fichier de sortie */
            resolution(row);
        }
        else if(choix == 1){

            do{
                printf("\nsaisir numero de codage (0 directe, 1 indirecte) :\n");
                scanf("%d", &numCodage);
            }while(numCodage<0 || numCodage>1);

            do{
            printf("\nsaisir numéro méthode : \n");
            printf("1 = tri par valeurs \n");
            printf("2 = aléatoire\n");
            printf("3 =  tri par ratio valeur/sommes des poids\n");
            printf("4 = tri par ratio valeur/dimension critique\n");
            printf("5 = tri par ratio valeur/dimension critique avec update dim critique\n");
            printf("6 = invention : tri par tailles des objets croissantes\n");
            printf("7 = recherche locale\n");
            printf("8 = recherche taboue \n");
            printf("9 = algorithme génétique \n");
            printf("10 = algorithme génétique + recherche locale\n");
            scanf("%d", &numMethode);
            }while(numMethode<1 || numMethode>10);

            resolution_choix(row, numMethode, numCodage);


        }
        else if(choix == 2)
            exit(EXIT_SUCCESS);

    }while(1);

    /** on détruit l'instanceDB */
    instanceDB_destroy(row);


    printf("\n");

    return 0;
}

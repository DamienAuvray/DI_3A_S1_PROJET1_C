#ifndef INSTANCE_H
#define INSTANCE_H

#include <stdio.h>
#include <stdlib.h>
#include "objet.h"

/**
* structure instance
* nbObjet : nombre d'objet dans l'instance
* nbDimension : nombre de dimension de l'instance
* listeObjet : liste de n objet permettant de récupérer leur valeur
    et leurs poids par dimension.
*/
typedef struct
{
    int nbObjet;
    int nbDimension;
    int solution1;
    int solution2;
    int *  sol;
    objet * listeObj;
    int * poidMax;


}instance;

typedef struct
{
    int nbInstance;
    instance ** listeInstance;
}instanceDB;

instance* instance_create_and_init(void);

void instance_destroy(instance * row);

void instanceDB_destroy(instanceDB * row);

instance * readInstance(FILE* fichier);



#endif // INSTANCE_H

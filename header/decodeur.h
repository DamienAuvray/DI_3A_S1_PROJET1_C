#ifndef DECODEUR_H_INCLUDED
#define DECODEUR_H_INCLUDED

#include "solution.h"
#include "instance.h"


solutionIndirecte * decoder(int * permutation, instance * row);

int ajoutSacPossible(instance * row, int numObj, int * capMax);

#endif // DECODEUR_H_INCLUDED

#ifndef SOLUTION_H_INCLUDED
#define SOLUTION_H_INCLUDED

#include "instance.h"

/**definition structure */
typedef struct{
    int * tabBinaire;
}solution;

typedef struct{
    int nbObjet;
    int * solution;
}solutionIndirecte;

/** fonction codage direct */
solution * solution_create(int nbObjet);

void solution_destroy(solution * sol);

void solution_copy(solution * sol1, solution * sol2, int nbObjet);


solution ** population_create(int taillePopu, instance * row);

void population_destroy(solution ** popu, int taillePopu);



void afficher_directe(solution *sol, instance *row);

int fonctionObjective_directe(solution *sol, instance *row);

int test_solution_directe(solution *sol, instance *row);


/** fonction codeage indirecte */
/** permutation et ensemble de permutation */
int * permutation_create(instance * row);

void permutation_init(int * perm, int nbObjet);

void permutation_copy(int * perm1, int * perm2, int nbObjet);

int ** permution_tab_create(instance * row, int taille);

void permutation_tab_destroy(int ** permu, int taillePopu);



/**solution et ensembles de solution */
solutionIndirecte * solutionIndirecte_create(int nbObjet);

void solutionIndirecte_destroy(solutionIndirecte* sol);

void solution_indirecte_copy(solutionIndirecte * sol1, solutionIndirecte * sol2);

solutionIndirecte ** population_indirecte_create(int taillePopu, instance * row);

void population_indirecte_destroy(solutionIndirecte ** popu, int taillePopu);


/** outil sur solutions */
void afficher_indirecte(solutionIndirecte *sol);

int fonctionObjective_indirecte(solutionIndirecte * sol, instance * row);


#endif // SOLUTION_H_INCLUDED

#ifndef ORDONNANCEMENT_H_INCLUDED
#define ORDONNANCEMENT_H_INCLUDED

#include "instance.h"


int * ordonnancement(int nbObjet, double * tabValeur);

double * listeValeurs(instance * row);
int * ordonnancement_valeur(instance *row);

int * ordonnancement_alea(instance *row);

double * listeRatioVal(instance * row);
int * ordonnancement_ratio_valeurObjet_sommePoids(instance * row);

double * listeRatioValCrit(instance * row);
int * ordonnancement_ratio_valeurObjet_dimensionCritique(instance * row);

int * updateDynamique(instance * row, int * liste, int * capMax, int nbObjet);

int * ordonnancement_invention(instance * row);

#endif // ORDONNANCEMENT_H_INCLUDED

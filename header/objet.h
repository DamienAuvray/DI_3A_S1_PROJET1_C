#ifndef OBJET_H
#define OBJET_H


/**
* structure objet
* valeur : la valeur de l'objet
* poids : liste de valeur pour chaque dimensions (pi)
    autant de de valeur que de dimension
*/
typedef struct{
    int valeur;
    int * listePoids;
}objet;


void objet_init(objet * obj);

void objet_destroy(objet * obj);

#endif // OBJET_H

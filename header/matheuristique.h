#ifndef MATHEURISTIQUE_H_INCLUDED
#define MATHEURISTIQUE_H_INCLUDED

#include "solution.h"

solution * rechercheLocale_directe(instance * row);

solutionIndirecte * rechercheLocale_indirecte(instance * row);

solution * rechercheTaboueDirecte(instance * row, int nbIteMax, int taille_listeTaboue, int aspi);

solutionIndirecte * rechercheTaboueIndirecte(instance * row, int nbIteMax, int taille_listeTaboue, int aspi);

/** recherhce locale prenant en entré une solution et la modifiant, pour l'algo génétique */

void rechercheLocale_directe_Invention(solution * sol, instance * row); //algo genetique avec recherche locale à la place de la mutation

void rechercheLocale_indirecte_invention(int * permu, solutionIndirecte * sol, instance * row);



#endif // MATHEURISTIQUE_H_INCLUDED

#ifndef GENETIQUE_H_INCLUDED
#define GENETIQUE_H_INCLUDED
#include "instance.h"
#include "solution.h"

/** gentique direct */
solution ** calculPopulation(instance * row, int taillePopu);

solution * meilleureSolution(solution ** popu, instance * row, int taillePopu);

void selectionParent(solution * P1, solution * P2, solution ** popu, int taillePopu, instance * row);

void creationEnfants(solution * E1, solution * E2, solution * P1, solution * P2, instance * row);

void optimisationEnfant(solution * E1, instance *row);

void reparation(solution * E1, instance * row);

void mutation(solution * sol, instance * row);

void renouvellement(solution** popCourante, solution ** popEnfant, instance * row, int taillePopu);

solution * algoGenetique_directe(instance * row, int nbIteMax, int taillePopu, double pMut);

/**genetique indirecte */
void calculPopulation_indirecte(int ** permutation, solutionIndirecte ** popu, instance * row, int taillePopu);

solutionIndirecte * meilleureSolution_indirecte(solutionIndirecte ** popu, instance * row, int taillePopu);

void selectionParent_indirecte(int * P1, int * P2, solutionIndirecte ** popu, int**permu, int taillePopu, instance * row);

void creationEnfants_indirecte(int * E1, int * E2, int * P1, int * P2, instance * row);

void mutation_indirecte(int * permu, solutionIndirecte * sol, instance * row);

void renouvellement_indirecte(int **permuCourante, int** permuEnfant, solutionIndirecte** popCourante, solutionIndirecte ** popEnfant, instance * row, int taillePopu);

solutionIndirecte * algoGenetique_indirecte(instance * row, int nbIteMax, int taillePopu, double pMut);


/** invention genetique + recherche locale */
solution * algoGenetique_directe_rechercheLocale(instance * row, int nbIteMax, int taillePopu, double pMut);

solutionIndirecte * algoGenetique_indirecte_rechercheLocale(instance * row, int nbIteMax, int taillePopu, double pMut);

#endif // GENETIQUE_H_INCLUDED

#ifndef HEURISTIQUE_H_INCLUDED
#define HEURISTIQUE_H_INCLUDED

#include "instance.h"
#include "solution.h"

solution * heuristiqueDirecte(instance * row, int numMethode);

solution * heuristiqueDirecteDynamique(instance * row);

#endif // HEURISTIQUE_H_INCLUDED

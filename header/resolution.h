#ifndef RESOLUTION_H_INCLUDED
#define RESOLUTION_H_INCLUDED

void resolution(instanceDB * row);

void resolution_choix(instanceDB * row, int numMethode, int numCodage);

solution * resolutionDirecte(instance * row, int numMethode, int nbIte, int tailleTaboue, int tailePopu, double pMut);

solutionIndirecte * resolutionIndirecte(instance * row, int methode, int nbIte, int tailleTaboue, int tailePopu, double pMut);

#endif // RESOLUTION_H_INCLUDED

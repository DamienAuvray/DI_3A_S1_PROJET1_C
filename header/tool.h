#ifndef TOOL_H
#define TOOL_H

#include "instance.h"

int * read_tab(FILE* fichier, int count);

void retirerElementTab(int * tab, int taille);

void init_tab(int * tab, int taille);

int trouverTabouIndirect(int * liste, int * mvt, int taille);

int trouverElement(int * liste, int element, int debut, int fin);

int trouverIndice(int * liste, int element, int debut, int fin);
#endif // TOOL_H

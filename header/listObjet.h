#ifndef LISTOBJET_H
#define LISTOBJET_H

#include <stdio.h>
#include <stdlib.h>
#include "objet.h"
#include "instance.h"

objet* objet_read_valeur(FILE* fichier, int nbObjet);

void objet_read_poids(FILE* fichier, instance * row);

#endif // LISTOBJET_H
